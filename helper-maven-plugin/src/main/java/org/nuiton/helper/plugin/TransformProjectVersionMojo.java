package org.nuiton.helper.plugin;

/*
 * #%L
 * Helper Maven Plugin :: Mojos
 * %%
 * Copyright (C) 2009 - 2012 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.nuiton.plugin.AbstractPlugin;

import java.util.Properties;

/**
 * Transform the version of the project and store it in a project property.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
@Mojo(name = "transform-project-version",
      defaultPhase = LifecyclePhase.INITIALIZE,
      aggregator = true,
      requiresProject = true)
public class TransformProjectVersionMojo extends AbstractPlugin {

    public static final String SNAPSHOT_SUFFIX = "-SNAPSHOT";

    /**
     * Project.
     *
     * @since 2.0
     */
    @Parameter( defaultValue = "${project}", required = true, readonly = true )
    private MavenProject project;

    /**
     * Scm revision to use.
     *
     * @since 2.0
     */
    @Parameter(property = "helper.revision", required = true)
    private String revision;


    /**
     * Pattern of the new version.
     *
     * Use {@code #V} for project version (without snapshot suffix) and
     * {@code #R} for the given scm revision and #S to add snapshot suffix
     *
     * Example:
     * <pre>
     * #V-rev-#V -> 1.0-rev-1 (version=1.0 and revision=1)
     * #V-rev-#V#S -> 1.0-rev-1-SNAPSHOT (version=1.0 and revision=1)
     * #V-rev-#V#S -> 1.0-rev-1-SNAPSHOT (version=1.0-SNAPSHOT and revision=1)
     * </pre>
     * Note that the #S can only be putted at the end of the pattern
     *
     * @since 2.0
     */
    @Parameter(property = "helper.newVersionPattern", defaultValue = "#V-rev-#R")
    private String newVersionPattern = "#V-rev-#R";

    /**
     * The project property where to store the version.
     *
     * @since 2.0
     */
    @Parameter(property = "helper.versionKey", defaultValue = "newVersion", required = true)
    private String versionKey = "newVersion";

    /**
     * A flag to activate verbose mode.
     *
     * @since 2.0
     */
    @Parameter(property = "helper.verbose", defaultValue = "${maven.verbose}")
    private boolean verbose;

    /**
     * A flag to execute only once the mojo.
     *
     * <b>Note:</b> By default, value is {@code true} since it is not necessary
     * to inject twice secrets in session.
     *
     * @since 1.1.0
     */
    @Parameter(property = "helper.runOnce", defaultValue = "true")
    private boolean runOnce;

    @Override
    public boolean checkSkip() {

        boolean result = true;
        if (runOnce) {

            // compute the unique key refering to parameters of plugin
            String key = "transform-project-version##" + revision;

            // check if plugin was already done.

            boolean shouldInvoke = needInvoke(true, false, key);

            result = shouldInvoke && super.checkSkip();
        }

        return result;

    }

    @Override
    protected void init() throws Exception {

        // validate the newVersionPattern
        if (!newVersionPattern.contains("#V") &&
            !newVersionPattern.contains("#R")) {
            throw new MojoExecutionException(
                    "newVersionPattern (" + newVersionPattern + ") must at least contains one of those patterns: *#V* or *#R*.");
        }

        if (newVersionPattern.contains("#S")) {

            // check that there is only one of it and at the end
            int firstIndex = newVersionPattern.indexOf("#S");
            int lastIndex = newVersionPattern.lastIndexOf("#S");

            if (firstIndex != lastIndex) {
                throw new MojoExecutionException(
                        "newVersionPattern (" + newVersionPattern + ") can only contains one pattern *#S*.");
            }

            if (lastIndex != newVersionPattern.length() - 2) {
                throw new MojoExecutionException(
                        "newVersionPattern (" + newVersionPattern + ") can only contains pattern *#S* at his end.");
            }
        }

        // See https://forge.nuiton.org/issues/3708
        if (revision.startsWith("#")) {

            revision = (String) getProject().getProperties().get(revision.substring(1));

        }

    }

    @Override
    protected void doAction() throws Exception {
        String version = getProject().getVersion();

        if (isVerbose()) {
            getLog().info("Initial version: " + version);
        }

        boolean snapshotFound = version.endsWith(SNAPSHOT_SUFFIX);
        if (snapshotFound) {

            // remove snapshot
            version = version.substring(0, version.length() - SNAPSHOT_SUFFIX.length());
        }

        String newVersion = newVersionPattern.replaceAll("#V", version);

        newVersion = newVersion.replaceAll("#R", revision);
        newVersion = newVersion.replaceAll("#S", SNAPSHOT_SUFFIX);

        if (isVerbose()) {
            getLog().info("New     version: " + newVersion);
        }

        Properties properties = project.getProperties();

        getLog().info("export newVersion [" + newVersion + "]  in ${" + versionKey + "}");

        properties.setProperty(versionKey, newVersion);
    }

    @Override
    public MavenProject getProject() {
        return project;
    }

    @Override
    public void setProject(MavenProject project) {
        this.project = project;
    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }
}
