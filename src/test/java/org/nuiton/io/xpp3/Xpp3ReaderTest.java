/*
 * #%L
 * Helper Maven Plugin
 * %%
 * Copyright (C) 2009 - 2010 Tony Chemit, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.io.xpp3;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.plugin.TestHelper;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * User: chemit
 * Date: 1 nov. 2009
 * Time: 19:50:39
 */
public class Xpp3ReaderTest {

    protected static File dir;

    protected Xpp3Reader<Identity> r = new IdentityXpp3Reader("identities");


    @BeforeClass
    public static void beforeClass() {

        List<String> paths = new ArrayList<String>();
        paths.add("target");
        paths.add("test-classes");
        paths.addAll(Arrays.asList(Xpp3HelperTest.class.getPackage().getName().split("\\.")));
        dir = TestHelper.getFile(TestHelper.getBasedir(), paths.toArray(new String[paths.size()]));
    }

    @Test
    public void testRead() throws Exception {

        File file = new File(dir, "identity.xml");

        Identity result;
        Reader input = new FileReader(file);
        try {
            Assert.assertNotNull(input);
            result = r.read(input);
        } finally {
            input.close();
        }
        Assert.assertNotNull(result);
        Identity expected = new Identity();
        expected.setId(1);
        expected.setFirstName("first name");
        expected.setLastName("last name");
        expected.setEmail("noway@bishop.uk");
        expected.setAge(29);
        CountryRef country = new CountryRef();
        country.setId(1);
        expected.setCountry(country);

        assertEqualsIdentity(result, expected);
    }


    @Test
    public void testReadArray() throws Exception {

        File file = new File(dir, "identities.xml");
        Reader input = new FileReader(file);
        try {
            Assert.assertNotNull(input);
            Identity[] result = r.readArray(input);
            assertEqualsIdentities(result);
        } finally {
            input.close();
        }
    }


    @Test
    public void testReadArray2() throws Exception {

        r = new IdentityXpp3Reader("identity");

        File file = new File(dir, "identities2.xml");

        Assert.assertTrue(file.exists());
        Identity[] result;
        Reader input = new FileReader(file);
        try {
            Assert.assertNotNull(input);
            result = r.readArray(input);
        } finally {
            input.close();
        }
        assertEqualsIdentities(result);

        r.setAddDefaultEntities(false);
        input = new FileReader(file);
        try {
            result = r.readArray(input);
        } finally {
            input.close();
        }

        assertEqualsIdentities(result);

        r.setAddDefaultEntities(true);
        input = new FileReader(file);
        try {
            result = r.readArray(input);
        } finally {
            input.close();
        }

        assertEqualsIdentities(result);


        r.setParentRootTagName("identities");

        input = new FileReader(new File(dir, "identities.xml"));
        try {
            Assert.assertNotNull(input);
            result = r.readArray(input);
        } finally {
            input.close();
        }

        assertEqualsIdentities(result);
    }

    protected static void assertEqualsIdentity(Identity result, Identity expected) {
        Assert.assertEquals(expected.getFirstName(), result.getFirstName());
        Assert.assertEquals(expected.getLastName(), result.getLastName());
        Assert.assertEquals(expected.getEmail(), result.getEmail());
        Assert.assertEquals(expected.getAge(), result.getAge());
        Assert.assertEquals(expected.getCountry(), result.getCountry());
    }


    protected static void assertEqualsSimpleIdentity(Identity result) {
        Assert.assertNotNull(result);
        Identity expected = new Identity();
        expected.setId(1);
        expected.setFirstName("first name");
        expected.setLastName("last name");
        expected.setEmail("noway@bishop.uk");
        expected.setAge(29);
        CountryRef country = new CountryRef();
        country.setId(1);
        expected.setCountry(country);

        assertEqualsIdentity(result, expected);
    }

    protected static void assertEqualsIdentities(Identity[] result) {
        Assert.assertNotNull(result);
        Assert.assertEquals(2, result.length);
        Identity expected = new Identity();
        expected.setId(1);
        expected.setFirstName("first name");
        expected.setLastName("last name");
        expected.setEmail("noway@bishop.uk");
        expected.setAge(29);
        CountryRef country = new CountryRef();
        country.setId(1);
        expected.setCountry(country);

        assertEqualsIdentity(result[0], expected);

        expected = new Identity();
        expected.setId(2);
        expected.setFirstName("first name2");
        expected.setLastName("last name2");
        expected.setEmail("noway2@bishop.uk");
        expected.setAge(31);
        country = new CountryRef();
        country.setId(2);
        expected.setCountry(country);

        assertEqualsIdentity(result[1], expected);
    }

}
