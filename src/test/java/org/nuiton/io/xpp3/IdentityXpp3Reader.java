/*
 * #%L
 * Helper Maven Plugin
 * %%
 * Copyright (C) 2009 - 2010 Tony Chemit, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.io.xpp3;

import java.beans.IntrospectionException;

/**
 * User: chemit
 * Date: 1 nov. 2009
 * Time: 20:37:56
 */
public class IdentityXpp3Reader extends AbstractXpp3Reader<Identity> {

    public IdentityXpp3Reader() {
        this("identities");
    }

    public IdentityXpp3Reader(String root) {
        super(Identity.class, root, "identity");
    }

    @Override
    protected void initMappers() throws IntrospectionException {

        addAttributeValueMappers(DefaultDataConverter.Integer, true,
                                 "id");

        addTagTextContentMappers(DefaultDataConverter.Text, true,
                                 "firstName",
                                 "lastName",
                                 "email");

        addTagTextContentMappers(DefaultDataConverter.Integer, true,
                                 "age");

        addTagContentMapper(CountryRef.class, true,
                            new CountryRefXpp3Reader(null, "country"));

    }

}
