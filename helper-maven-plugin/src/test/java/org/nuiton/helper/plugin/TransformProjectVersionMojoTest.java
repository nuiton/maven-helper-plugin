package org.nuiton.helper.plugin;

/*
 * #%L
 * Helper Maven Plugin :: Mojos
 * %%
 * Copyright (C) 2009 - 2012 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.plugin.MojoTestRule;

import java.io.File;
import java.util.Properties;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.0
 */
public class TransformProjectVersionMojoTest {

    /** Logger. */
    private static final Log log = LogFactory.getLog(TransformProjectVersionMojoTest.class);

    protected boolean canContinue;

    @Rule
    public MojoTestRule<TransformProjectVersionMojo> rule = new MojoTestRule<TransformProjectVersionMojo>(TransformProjectVersionMojoTest.class, "transform-project-version") {

        @Override
        protected void setUpMojo(TransformProjectVersionMojo mojo, File pomFile) throws Exception {

            super.setUpMojo(mojo, pomFile);

            canContinue = initMojo(mojo);

        }
    };

    @Test
    public void simple() throws Exception {

        Assert.assertTrue(canContinue);
        TransformProjectVersionMojo mojo = rule.getMojo();
        Assert.assertNotNull(mojo);
        mojo.doAction();

        assertNewVersion("newVersion", "1.0-rev-1");

    }

    @Test
    public void keepSnapshot() throws Exception {

        Assert.assertTrue(canContinue);
        TransformProjectVersionMojo mojo = rule.getMojo();
        Assert.assertNotNull(mojo);
        mojo.doAction();

        assertNewVersion("newVersion", "2.0-rev-2-SNAPSHOT");

    }

    @Test
    public void removeSnapshot() throws Exception {

        Assert.assertTrue(canContinue);
        TransformProjectVersionMojo mojo = rule.getMojo();
        Assert.assertNotNull(mojo);
        mojo.doAction();

        assertNewVersion("newVersion", "2.0-rev-2");

    }

    @Test
    public void badVersionPattern1() throws Exception {

        Assert.assertFalse(canContinue);

    }

    @Test
    public void badVersionPattern2() throws Exception {

        Assert.assertFalse(canContinue);

    }

    @Test
    public void badVersionPattern3() throws Exception {

        Assert.assertFalse(canContinue);

    }

    protected void assertNewVersion(String versionKey, String expectedNewVersion) {

        Properties properties = rule.getMojo().getProject().getProperties();
        Object newVersion = properties.get(versionKey);
        Assert.assertNotNull(newVersion);
        Assert.assertEquals(expectedNewVersion, newVersion);

    }

}
