/*
 * #%L
 * Helper Maven Plugin
 * %%
 * Copyright (C) 2009 - 2010 Tony Chemit, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.io.xpp3;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.Iterator;

/**
 * User: chemit
 * Date: 1 nov. 2009
 * Time: 20:34:34
 */
public class Xpp3HelperTest {


    @BeforeClass
    public static void beforeClass() {
        Xpp3ReaderTest.beforeClass();
    }

    @Test
    public void testGetReader() throws Exception {
        Xpp3Reader<Identity> r = Xpp3Helper.getReader(Identity.class);
        Assert.assertNotNull(r);
    }

    @Test
    public void testGetReaderIterator() throws Exception {
        Iterator<Xpp3Reader<?>> r = Xpp3Helper.getReaderItetator();
        Assert.assertNotNull(r);
        Assert.assertTrue(r.hasNext());
        Xpp3Reader<?> reader = r.next();
        Assert.assertNotNull(reader);
    }

    @Test(expected = NullPointerException.class)
    public void testReadObjectFailed() throws Exception {

        Xpp3Helper.readObject(String.class, null);
    }

    @Test(expected = NullPointerException.class)
    public void testReadObjectFailed2() throws Exception {

        File file = new File(Xpp3ReaderTest.dir, "identity.xml");
        Assert.assertTrue(file.exists());

        Reader input = new FileReader(file);

        try {
            Assert.assertNotNull(input);

            Xpp3Helper.readObject(null, input);
        } finally {
            input.close();
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testReadObjectFailed3() throws Exception {

        File file = new File(Xpp3ReaderTest.dir, "identity.xml");
        Assert.assertTrue(file.exists());

        Reader input = new FileReader(file);
        try {
            Assert.assertNotNull(input);

            Xpp3Helper.readObject(String.class, input);
        } finally {
            input.close();
        }
    }

    @Test
    public void testReadObject() throws Exception {

        File file = new File(Xpp3ReaderTest.dir, "identity.xml");
        Assert.assertTrue(file.exists());

        Identity result;
        Reader input = new FileReader(file);
        try {
            Assert.assertNotNull(input);

            result = Xpp3Helper.readObject(Identity.class, input);
        } finally {
            input.close();
        }

        Xpp3ReaderTest.assertEqualsSimpleIdentity(result);
    }


    @Test(expected = NullPointerException.class)
    public void testReadObjectsFailed() throws Exception {

        Xpp3Helper.readObjects(String.class, null);
    }


    @Test(expected = NullPointerException.class)
    public void testReadObjectsFailed2() throws Exception {

        File file = new File(Xpp3ReaderTest.dir, "identity.xml");
        Assert.assertTrue(file.exists());

        Reader input = new FileReader(file);
        try {
            Assert.assertNotNull(input);

            Xpp3Helper.readObjects(null, input);
        } finally {
            input.close();
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testReadObjectsFailed3() throws Exception {

        File file = new File(Xpp3ReaderTest.dir, "identity.xml");
        Assert.assertTrue(file.exists());

        Reader input = new FileReader(file);
        try {
            Assert.assertNotNull(input);

            Xpp3Helper.readObjects(String.class, input);
        } finally {
            input.close();
        }
    }

    @Test
    public void testReadObjects() throws Exception {

        File file = new File(Xpp3ReaderTest.dir, "identities.xml");
        Identity[] result;
        Reader input = new FileReader(file);
        try {
            Assert.assertNotNull(input);
            result = Xpp3Helper.readObjects(Identity.class, input);
        } finally {
            input.close();
        }
        Xpp3ReaderTest.assertEqualsIdentities(result);
    }

    @Test
    public void testCleanReaders() throws Exception {
        Xpp3Helper.clearReaders();
        Assert.assertNull(Xpp3Helper.readers);
    }
}
