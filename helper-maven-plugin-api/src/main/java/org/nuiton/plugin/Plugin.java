package org.nuiton.plugin;

/*
 * #%L
 * Helper Maven Plugin :: API
 * %%
 * Copyright (C) 2009 - 2012 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.maven.plugin.Mojo;
import org.apache.maven.project.MavenProject;

/**
 * A common contract to be implements by our mojo and reports.
 *
 * Just expose a {@link #isVerbose()} flag and the maven project {@link #getProject()}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.3
 */
public interface Plugin extends Mojo {

    /** An enumeration to qualify the init result of a plugin */
    enum InitState {

        /** when something is wrong and should throw an error */
        failed,

        /** when the plugin execution should be skipped */
        skip,

        /** when plugin can be executed */
        ok
    }

    /** An enumeration to qualify a maven module packaging */
    enum Packaging {

        pom,
        jar,
        plugin,
        war,
        ear
    }

    MavenProject getProject();

    void setProject(MavenProject project);

    boolean isVerbose();

    void setVerbose(boolean verbose);
}
