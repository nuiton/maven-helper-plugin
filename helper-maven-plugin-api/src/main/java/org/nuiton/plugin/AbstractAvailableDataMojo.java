package org.nuiton.plugin;

/*
 * #%L
 * Helper Maven Plugin :: API
 * %%
 * Copyright (C) 2009 - 2012 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

import java.util.Collection;
import java.util.Map;

/**
 * Abstract mojo used to display {@link AvailableData} in a simple way.
 *
 * Created: 10/05/11
 *
 * @author Florian Desbois
 * @since 1.3
 */
public abstract class AbstractAvailableDataMojo extends AbstractMojo {

    /**
     * AvailableData type that contains all data needed to display and how
     * to display them. Could easily be match over an enumeration.
     */
    protected interface AvailableData {

        /**
         * @return name of the dataType
         */
        String name();

        /**
         * @return map of all available data
         */
        Map<String, ?> getData();

        /**
         * @param value Data value
         * @return how to display a data {@code value} as a String
         */
        String toString(Object value);

    }

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {

        StringBuilder buffer = new StringBuilder();

        Collection<AvailableData> safeDataTypes = getAllAvailableDatas();

        for (AvailableData data : safeDataTypes) {
            buffer.append("\n");
            appendData(data, buffer);
        }
        buffer.append("\n");

        // Display info
        String info;
        if (safeDataTypes.size() == 1) {
            info = buffer.toString();
        } else {
            info = "Get datas for data types : " + safeDataTypes +
                      buffer.toString();
        }
        getLog().info(info);
    }

    /**
     * Append a {@code data} to the given {@code buffer}.
     *
     * @param data AvailableData to append
     * @param buffer StringBuilder to use
     */
    protected void appendData(AvailableData data, StringBuilder buffer) {

        Map<String, ?> map = data.getData();

        int size = map.size();
        String dataType = data.name();
        if (size == 0) {
            buffer.append("\nNo available ").append(dataType).append(".");
        } else if (size == 1) {
            buffer.append("\nFound one ").append(dataType).append(" : ");
        } else {
            buffer.append("\nFound ");
            buffer.append(size);
            buffer.append(" ");
            buffer.append(dataType);
            buffer.append("s : ");
        }
        for (Map.Entry<String, ?> e : map.entrySet()) {
            String name = e.getKey();
            Object value = e.getValue();
            buffer.append("\n [");
            buffer.append(name);
            buffer.append("] with implementation '");
            buffer.append(data.toString(value));
            buffer.append('\'');
        }
    }

    /**
     * Retrieve the Collection of all AvailableData to display.
     *
     * @return the Collection of all {@link AvailableData}.
     */
    protected abstract Collection<AvailableData> getAllAvailableDatas();

}
