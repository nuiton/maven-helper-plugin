package org.nuiton.helper.plugin;

/*
 * #%L
 * Helper Maven Plugin :: Mojos
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.plugin.MojoTestRule;

/**
 * Created on 5/25/15.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.2
 */
public class CheckAutoContainerPluginTest {

    @Rule
    public MojoTestRule<CheckAutoContainerPlugin> rule =
            MojoTestRule.newRule(CheckAutoContainerPluginTest.class, "check-auto-container");

    //FIXME Got a NPE in artifact resolver (session is null) :(
    /*
    java.lang.NullPointerException
	at org.apache.maven.artifact.resolver.DefaultArtifactResolver.resolve(DefaultArtifactResolver.java:208)
	at org.apache.maven.artifact.resolver.DefaultArtifactResolver.resolve(DefaultArtifactResolver.java:401)
	at org.nuiton.helper.plugin.CheckAutoContainerPlugin.checkDependency(CheckAutoContainerPlugin.java:461)
     */
    @Ignore
    @Test
    public void central() throws Exception {

        CheckAutoContainerPlugin mojo = rule.getMojo();
        Assert.assertNotNull(mojo);
        boolean canContinue = rule.initMojo(mojo);
        Assume.assumeTrue(canContinue);

        mojo.doAction();

    }

}