package org.nuiton.plugin;

/*
 * #%L
 * Helper Maven Plugin :: API
 * %%
 * Copyright (C) 2009 - 2012 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2.3
 */
public class PluginHelperTest {

    /** Logger */
    static private Log log = LogFactory.getLog(PluginHelperTest.class);

    protected File getFile() throws URISyntaxException {
        URL resource = getClass().getResource("urls.txt");
        File file = new File(resource.toURI().getPath());
        log.info("file to read " + file);
        Assert.assertTrue("Could not find file " + file, file.exists());
        return file;
    }

    @Test
    public void testGetLines() throws Exception {
        File file = getFile();
        String[] lines = PluginHelper.getLines(file);
        Assert.assertNotNull(lines);
        Assert.assertEquals(1, lines.length);
        Assert.assertEquals("file:///file.txt", lines[0]);
    }

    @Test
    public void testGetLinesAsURL() throws Exception {
        File file = getFile();
        URL[] urls = PluginHelper.getLinesAsURL(file);
        Assert.assertNotNull(urls);
        Assert.assertEquals(1, urls.length);
        Assert.assertEquals(new URL("file:///file.txt"), urls[0]);
    }

    @Test
    public void testGetLinesAsFiles() throws Exception {
        File file = getFile();
        File[] files = PluginHelper.getLinesAsFiles(file);
        Assert.assertNotNull(files);
        Assert.assertEquals(1, files.length);
        Assert.assertEquals(new File("file:///file.txt"), files[0]);
    }

}
