/*
 * #%L
 * Helper Maven Plugin
 * %%
 * Copyright (C) 2009 - 2012 Tony Chemit, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.io.xpp3;

/**
 * Define a country ref
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.5
 */
public class CountryRef {
    
    protected int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CountryRef)) return false;

        CountryRef that = (CountryRef) o;

        return id == that.id;

    }

    @Override
    public int hashCode() {
        return id;
    }
}
