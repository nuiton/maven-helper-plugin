package org.nuiton.helper.plugin;

/*
 * #%L
 * Helper Maven Plugin :: Mojos
 * %%
 * Copyright (C) 2009 - 2012 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.model.Developer;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.logging.Logger;
import org.codehaus.plexus.logging.console.ConsoleLogger;
import org.codehaus.plexus.mailsender.MailMessage;
import org.codehaus.plexus.mailsender.MailSenderException;
import org.nuiton.helper.io.mail.MailSender;
import org.nuiton.helper.io.mail.ProjectJavamailMailSender;
import org.nuiton.plugin.AbstractPlugin;
import org.nuiton.plugin.PluginHelper;
import org.nuiton.plugin.PluginWithEncoding;

import java.io.File;
import java.util.List;

/**
 * Send a email.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.3
 */
@Mojo(name = "send-email", requiresOnline = true, requiresProject = true)
public class SendEmailMojo extends AbstractPlugin implements PluginWithEncoding {

    /**
     * Dependance du projet.
     *
     * @since 1.0.3
     */
    @Parameter( defaultValue = "${project}", required = true, readonly = true )
    protected MavenProject project;

    /**
     * Un flag pour activer le mode verbeux.
     *
     * @since 1.0.3
     */
    @Parameter(property = "helper.verbose", defaultValue = "${maven.verbose}")
    protected boolean verbose;

    /**
     * The Velocity template used to format the email announcement.
     *
     * @since 1.0.3
     */
    @Parameter(property = "helper.emailContentFile", required = true)
    protected File emailContentFile;

    /**
     * The title of the email to send.
     *
     * @since 1.0.3
     */
    @Parameter(property = "helper.emailTitle", required = true)
    protected String emailTitle;

    /**
     * Smtp Server.
     *
     * @since 1.0.3
     */
    @Parameter(property = "helper.smtpHost", required = true)
    protected String smtpHost;

    /**
     * Port.
     *
     * @since 1.0.3
     */
    @Parameter(property = "helper.smtpPort", defaultValue = "25", required = true)
    protected int smtpPort;

    /**
     * The username used to send the email.
     *
     * @since 1.0.3
     */
    @Parameter(property = "helper.username")
    protected String username;

    /**
     * The password used to send the email.
     *
     * @since 1.0.3
     */
    @Parameter(property = "helper.password")
    protected String password;

    /**
     * If the email should be sent in SSL mode.
     *
     * @since 1.0.3
     */
    @Parameter(property = "helper.sslMode", defaultValue = "false")
    protected boolean sslMode;

    /**
     * The id of the developer sending the announcement mail. Only used if
     * the <i>mailSender</i> attribute is not set. In this case, this should
     * match the id of one of the developers in the pom. If a matching
     * developer is not found, then the first developer in the pom will be
     * used.
     *
     * @since 1.0.3
     */
    @Parameter(property = "helper.fromDeveloperId")
    protected String fromDeveloperId;

    /**
     * Defines the sender of the announcement if the list of developer is empty
     * or if the sender is not a member of the development team.
     *
     * @since 1.0.3
     */
    @Parameter(property = "helper.mailSender")
    protected MailSender mailSender;

    /**
     * Recipient email address.
     *
     * @since 1.0.3
     */
    @Parameter(required = true)
    protected List<String> toAddresses;

    /**
     * Possible senders.
     *
     * @since 1.0.3
     */
    @Parameter(property = "project.developers", required = true, readonly = true)
    protected List<Developer> from;

    /**
     * Mail content type to use.
     *
     * @since 1.0.3
     */
    @Parameter(defaultValue = "text/plain", required = true)
    protected String mailContentType;

    /**
     * The encoding used to read and write files.
     *
     * @since 1.0.3
     */
    @Parameter(property = "helper.encoding", defaultValue = "${project.build.sourceEncoding}")
    protected String encoding;

    /**
     * A flag to skip the goal.
     *
     * @since 1.0.3
     */
    @Parameter(property = "helper.skipSendEmail", defaultValue = "false")
    protected boolean skipSendEmail;

    /**
     * A flag to test plugin but send nothing to redmine.
     *
     * @since 1.0.3
     */
    @Parameter(property = "dryRun", defaultValue = "false")
    protected boolean dryRun;

    /**
     * A flag to restirct only one run in a build (for multi-module context).
     *
     * @since 1.0.3
     */
    @Parameter(property = "helper.runOnce", defaultValue = "true")
    protected boolean runOnce;

    /**
     * A flag to restrict only to run on root module.
     *
     * @since 2.1
     */
    @Parameter(property = "helper.runOnlyOnRoot", defaultValue = "true")
    protected boolean runOnlyOnRoot;

    ///////////////////////////////////////////////////////////////////////////
    /// Plugin
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public MavenProject getProject() {
        return project;
    }

    @Override
    public void setProject(MavenProject project) {
        this.project = project;
    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    ///////////////////////////////////////////////////////////////////////////
    /// AbstractPlugin
    ///////////////////////////////////////////////////////////////////////////

    @Override
    protected void init() throws Exception {

        if (isGoalSkip()) {
            return;
        }

        if (!emailContentFile.exists()) {
            // no file to publish...
            throw new MojoExecutionException("could not find the template " + emailContentFile);
        }

        // check mail sender

        if (mailSender == null) {

            if (from == null || from.isEmpty()) {
                throw new MojoExecutionException(
                        "The <developers> section in your pom should " +
                        "not be empty. Add a <developer> entry or set " +
                        "the " + "mailSender parameter.");
            }

            if (fromDeveloperId == null) {
                Developer dev = from.get(0);
                mailSender = new MailSender(dev.getName(), dev.getEmail());
            } else {
                for (Developer developer : from) {
                    if (fromDeveloperId.equals(developer.getId())) {
                        mailSender = new MailSender(developer.getName(), developer.getEmail());
                        break;
                    }
                }
                if (mailSender == null) {
                    throw new MojoExecutionException(
                            "Missing developer with id '" + fromDeveloperId + "' in the <developers> section in your pom.");
                }
            }
        }

        String fromAddress = mailSender.getEmail();
        if (StringUtils.isEmpty(fromAddress)) {
            throw new MojoExecutionException(
                    "Invalid mail sender: name and email is mandatory (" + mailSender + ").");
        }

        if (StringUtils.isBlank(emailTitle)) {
            throw new MojoExecutionException("Mail title can not be blank.");
        }

        emailTitle = emailTitle.trim();
    }

    @Override
    protected boolean checkSkip() {
        if (isGoalSkip()) {
            // goal mark to be skipped
            getLog().info("Skipping goal (skipSendEmail flag is on).");
            return false;
        }

        // check if plugin was already done

        StringBuilder buffer = new StringBuilder("send-email");
        buffer.append("##").append(emailTitle);
        buffer.append("##").append(emailContentFile);
        buffer.append("##").append(mailSender.getEmail());
        for (String toAddress : toAddresses) {
            buffer.append("##").append(toAddress);
        }
        buffer.append("##").append(mailSender.getEmail());

        String key = buffer.toString();
        boolean shouldInvoke = needInvoke(runOnce, runOnlyOnRoot, key);

        if (!shouldInvoke) {
            getLog().info("Skipping goal (runOnce flag is on and goal was already executed).");
        }
        return shouldInvoke && super.checkSkip();

    }

    @Override
    protected void doAction() throws Exception {

        if (dryRun) {
            getLog().info("\n  dryRun flag is on, no mail will be send!\n");
        }

        String newsContent =
                PluginHelper.readAsString(emailContentFile, encoding);

        MailMessage mailMsg = createMessage(newsContent, mailSender);


        ProjectJavamailMailSender mailer = createMailer();

        if (getLog().isDebugEnabled()) {
            getLog().debug("fromDeveloperId: " + fromDeveloperId);
        }

        if (dryRun) {
            getLog().info("Mail title   : " + emailTitle);
            getLog().info("Mail content :\n" + mailMsg.getContent());
            return;
        }
        getLog().info("Connecting to Host: " + smtpHost + ":" + smtpPort);

        try {

            mailer.send(mailMsg);
            getLog().info("Sent...");
        } catch (MailSenderException e) {
            throw new MojoExecutionException(
                    "Failed to send email for reason " + e.getMessage(), e);
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    /// Other
    ///////////////////////////////////////////////////////////////////////////

    protected boolean isGoalSkip() {
        return skipSendEmail;
    }

    protected MailMessage createMessage(String newsContent, MailSender mailSender) throws MailSenderException {

        String fromName = mailSender.getName();
        String fromAddress = mailSender.getEmail();

        getLog().info("Using this sender for email announcement: " + fromAddress + " < " + fromName + " > ");

        MailMessage mailMsg = new MailMessage();
        mailMsg.setSubject(emailTitle);
        if (getLog().isDebugEnabled()) {
            getLog().debug("email announcement :\n" + newsContent);
        }
        mailMsg.setContent(newsContent);
        mailMsg.setContentType(mailContentType);
        mailMsg.setFrom(fromAddress, fromName);
        for (String e : toAddresses) {
            getLog().info("Sending mail to " + e + "...");
            mailMsg.addTo(e, "");
        }
        return mailMsg;
    }

    protected ProjectJavamailMailSender createMailer() {

        ProjectJavamailMailSender mailer = new ProjectJavamailMailSender();
        ConsoleLogger logger = new ConsoleLogger(Logger.LEVEL_INFO, "base");
        if (isVerbose()) {
            logger.setThreshold(Logger.LEVEL_DEBUG);
        }
        mailer.enableLogging(logger);
        mailer.setSmtpHost(smtpHost);
        mailer.setSmtpPort(smtpPort);
        mailer.setSslMode(sslMode);
        if (username != null) {
            mailer.setUsername(username);
        }
        if (password != null) {
            mailer.setPassword(password);
        }
        mailer.initialize();
        return mailer;
    }

    public void setMailSender(MailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void setFrom(List<Developer> from) {
        this.from = from;
    }

    public void setToAddresses(List<String> toAddresses) {
        this.toAddresses = toAddresses;
    }

    @Override
    protected boolean isExecutionRoot() {
        return project.isExecutionRoot();
    }

    @Override
    public String getEncoding() {
        return encoding;
    }

    @Override
    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }
}
