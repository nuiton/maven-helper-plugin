package org.nuiton.plugin;

/*
 * #%L
 * Helper Maven Plugin :: API
 * %%
 * Copyright (C) 2009 - 2012 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.File;

/**
 * Une classe pour définir les entrées-sortie d'un plugin.
 *
 * En entrée, on peut avoir un ou plusieurs répertoires ({@link #inputs}.
 *
 * En sortie, on ne peut avoir qu'un seul répertoire ({@link #output}.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.1
 */
public class PluginIOContext {

    protected File[] inputs;

    protected File output;

    public File[] getInputs() {
        return inputs;
    }

    public File getOutput() {
        return output;
    }

    public void setInput(File input) {
        inputs = new File[]{input};
    }

    public void setInputs(File[] inputs) {
        this.inputs = inputs;
    }

    public void setOutput(File output) {
        this.output = output;
    }
}
