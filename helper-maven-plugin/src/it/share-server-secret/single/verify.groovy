/*
 * #%L
 * Helper Maven Plugin :: Mojos
 * %%
 * Copyright (C) 2009 - 2012 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
assert new File(basedir, 'build.log').exists();

content = new File(basedir, 'build.log').text;

assert content.contains( 'username = serverOne-username' );
assert content.contains( 'password = serverOne-password' );
assert content.contains( 'passphrase = serverOne-passphrase' );
assert content.contains( 'privateKey = serverOne-privateKey' );
assert content.contains( 'username2 = serverTwo-username' );
assert content.contains( 'password2 = serverTwo-password' );
assert content.contains( 'passphrase2 = serverTwo-passphrase' );
assert content.contains( 'privateKey2 = serverTwo-privateKey' );

return true;
