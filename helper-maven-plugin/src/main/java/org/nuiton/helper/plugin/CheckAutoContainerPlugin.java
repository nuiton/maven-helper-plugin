package org.nuiton.helper.plugin;

/*
 * #%L
 * Helper Maven Plugin :: Mojos
 * %%
 * Copyright (C) 2009 - 2012 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.ArtifactUtils;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.repository.ArtifactRepositoryFactory;
import org.apache.maven.artifact.repository.layout.ArtifactRepositoryLayout;
import org.apache.maven.artifact.repository.layout.DefaultRepositoryLayout;
import org.apache.maven.artifact.resolver.ArtifactResolutionRequest;
import org.apache.maven.artifact.resolver.ArtifactResolutionResult;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.apache.maven.settings.Proxy;
import org.apache.maven.wagon.proxy.ProxyInfo;
import org.codehaus.plexus.util.StringUtils;
import org.nuiton.plugin.AbstractPlugin;
import org.nuiton.plugin.PluginHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Check all dependencies are auto contained in the given repositories.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.2.5
 */
@Mojo(name = "check-auto-container",
        defaultPhase = LifecyclePhase.VALIDATE,
        requiresProject = true,
        requiresOnline = true,
        requiresDependencyResolution = ResolutionScope.RUNTIME)
public class CheckAutoContainerPlugin extends AbstractPlugin {

    /**
     * Map of repositories to use.
     *
     * Keys are repository id and Values are repository url.
     *
     * @since 1.2.5
     */
    @Parameter
    protected Map<String, String> repositories;

    /**
     * A flag to add the maven central repository https://repo1.maven.org/maven2
     * to {@link #repositories}.
     *
     * @since 1.2.5
     */
    @Parameter(property = "addMavenCentral", defaultValue = "false")
    protected boolean addMavenCentral;

    /**
     * A flag to fail if project is not auto container.
     *
     * @since 1.2.5
     */
    @Parameter(property = "helper.failIfNotSafe", defaultValue = "false")
    protected boolean failIfNotSafe;

    /**
     * A flag to activate verbose mode.
     *
     * @since 1.2.5
     */
    @Parameter(property = "helper.verbose", defaultValue = "${maven.verbose}")
    protected boolean verbose;

    /**
     * A flag to execute only once the mojo.
     *
     * <b>Note:</b> By default, value is {@code true} since it is not necessary to check twice for a same artifact.
     *
     * @since 1.2.6
     */
    @Parameter(property = "helper.runOnce", defaultValue = "true")
    protected boolean runOnce;

    /**
     * A flag to restrict only to run on root module.
     *
     * @since 2.1
     */
    @Parameter(property = "helper.runOnlyOnRoot", defaultValue = "true")
    protected boolean runOnlyOnRoot;

    /**
     * A flag to skip the goal.
     *
     * @since 2.1
     */
    @Parameter(property = "helper.skipCheckAutocontainer", defaultValue = "false")
    protected boolean skipCheckAutocontainer;

    /**
     * Project.
     *
     * @since 1.2.5
     */
    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    protected MavenProject project;

    /**
     * The projects in reactor (used to detected sibling dependencies).
     *
     * @since 1.2.5
     */
    @Parameter(property = "reactorProjects", readonly = true)
    protected List<?> reactorProjects;

    /**
     * Active proxy from settings (if any).
     *
     * @since 1.2.5
     */
    @Parameter(property = "settings.activeProxy", required = true, readonly = true)
    protected Proxy proxy;

    /**
     * Artifact repository factory component.
     *
     * @since 1.2.5
     */
    @Component
    protected ArtifactRepositoryFactory artifactRepositoryFactory;

    /**
     * Wagon manager component.
     *
     * @since 1.2.5
     */
    @Component
    protected ArtifactResolver artifactResolver;

    /**
     * Authorized Remote Repositories.
     *
     * @since 1.2.5
     */
    private List<ArtifactRepository> safeRepositories;

    /**
     * List of artifacts to treate.
     *
     * @since 1.2.5
     */
    private List<Artifact> artifacts;

    /**
     * Dictionnary of already resolved artifacts (keys are repository url and values are list of artifact ids).
     *
     * @since 1.2.5
     */
    private static Map<String, List<String>> resolved;

    public static final String MAVEN_CENTRAL_ID = "maven-central";

    public static final String MAVEN_CENTRAL_URL = "https://repo1.maven.org/maven2/";

    @Override
    public MavenProject getProject() {
        return project;
    }

    @Override
    public void setProject(MavenProject project) {
        this.project = project;
    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    @Override
    public void init() throws Exception {

        if (isGoalSkip()) {
            return;
        }

        Log log = getLog();

        if (log.isDebugEnabled()) {

            // always be verbose in debug mode
            setVerbose(true);
        }

        safeRepositories = createSafeRepositories();

        artifacts = prepareArtifacts();

        if (isVerbose()) {
            log.info(artifacts.size() + " detected dependencies : ");
            for (Object artifact : artifacts) {
                log.info(" - " + artifact);
            }
        }
    }

    @Override
    protected boolean checkSkip() {

        if (isGoalSkip()) {
            // goal mark to be skipped
            getLog().info("Skipping goal (skipCheckAutocontainer flag is on).");
            return false;
        }


        StringBuilder buffer = new StringBuilder("check-auto-container##");
        Artifact artifact = project.getArtifact();
        buffer.append(artifact.getGroupId()).append(":");
        buffer.append(artifact.getArtifactId()).append(":");
        buffer.append(artifact.getVersion()).append("##");

        // check if plugin was already done.

        String key = buffer.toString();

        boolean shouldInvoke = needInvoke(runOnce, runOnlyOnRoot, key);

        if (!shouldInvoke) {
            getLog().info("Skipping goal (runOnce flag is on and goal was already executed).");
            return false;
        }

        if (artifacts.isEmpty()) {
            getLog().info("Skipping goal (Project has no dependecy).");
            return false;
        }

        if (repositories.isEmpty()) {
            getLog().info("Skipping goal (No repository defined).");
            return false;
        }
        return super.checkSkip();
    }

    @Override
    protected void doAction() throws Exception {

        Log log = getLog();

        log.info(artifacts.size() + " dependencies to check.");

        for (ArtifactRepository repository : safeRepositories) {

            if (artifacts.isEmpty()) {

                // no more artifacts to check
                continue;
            }

            long t0 = System.nanoTime();
            String url = repository.getUrl();

            List<String> ids = getRepositoryCache(url);

            if (verbose) {
                log.info("Use repository " + url);
            }
            List<Artifact> found = checkDependency(repository, ids);
            long delay = System.nanoTime() - t0;
            String message;
            if (found.isEmpty()) {
                message = "No artifact resolved by the repository " + url;
            } else {
                message = String.format("%1$4s artifact(s) resolved by repository %2$s in %3$s", found.size(), url, PluginHelper.convertTime(delay));
            }
            log.info(message);
            artifacts.removeAll(found);

        }

        boolean safe = artifacts.isEmpty();

        if (safe) {
            log.info("All dependencies are safe.");
            return;
        }

        log.warn("There is " + artifacts.size() + " unsafe dependencie(s) :");
        for (Object artifact : artifacts) {
            log.warn(" - " + artifact);
        }
        if (failIfNotSafe) {
            throw new MojoFailureException("There is still some unsafe dependencies.");
        }
    }

    protected List<ArtifactRepository> createSafeRepositories() {

        List<ArtifactRepository> result = new ArrayList<>();

        ArtifactRepositoryLayout repositoryLayout = new DefaultRepositoryLayout();
        if (this.repositories == null) {
            this.repositories = new TreeMap<>();
        }
        List<String> ids = new ArrayList<>(this.repositories.keySet());
        if (addMavenCentral) {

            ids.add(0, MAVEN_CENTRAL_ID);
            this.repositories.put(MAVEN_CENTRAL_ID, MAVEN_CENTRAL_URL);
        }

        for (String id : ids) {

            String url = this.repositories.get(id).trim();

            ArtifactRepository repo = artifactRepositoryFactory.createDeploymentArtifactRepository(
                    String.valueOf(id), url, repositoryLayout, true);

            getLog().info("Will use repository " + repo.getUrl());
            if (verbose) {
                getLog().info(repo.toString());
            }
            result.add(repo);
        }

        return result;

    }

    protected List<Artifact> prepareArtifacts() {

        List<Artifact> result = new ArrayList<>();

        List<String> siblings = new ArrayList<>();

        if (reactorProjects != null) {
            for (Object o : reactorProjects) {
                MavenProject m = (MavenProject) o;
                siblings.add(getArtifactId(m.getArtifact()));
            }
        }

        // treate classic dependencies
        for (Object o : project.getArtifacts()) {
            Artifact a = (Artifact) o;
            Artifact artifact = acceptArtifact(a, siblings);
            if (artifact != null) {
                result.add(a);
            }
        }

        // treate also plugin dependencies
        for (Object o : project.getPluginArtifacts()) {
            Artifact a = (Artifact) o;
            Artifact artifact = acceptArtifact(a, siblings);
            if (artifact != null) {
                result.add(a);
            }
        }
        return result;
    }

    protected Artifact acceptArtifact(Artifact a, List<String> siblings) {
        String id = getArtifactId(a);

        if (siblings.contains(id)) {

            // skip a sibling dependency
            if (verbose) {
                getLog().info("Skip sibling dependency : " + id);
            }
            return null;
        }

        if (a.isSnapshot()) {

            // skip snapshot
            getLog().warn("Skip SNAPSHOT dependency : " + id);
            return null;
        }

        Artifact artifact = ArtifactUtils.copyArtifact(a);

        // force artifact not to be treated
        artifact.setResolved(false);

        // will treate this artifact
        return artifact;
    }

    private List<Artifact> checkDependency(ArtifactRepository repo, List<String> ids) throws Exception {

        Log log = getLog();
        List<Artifact> result = new ArrayList<>();

        for (Artifact artifact : artifacts) {
            if (log.isDebugEnabled()) {
                log.debug(" - check artifact : " + artifact);
            }
            String id = getArtifactId(artifact);

            boolean found;

            boolean inCache = ids.contains(id);
            if (inCache) {

                // already resolved
                found = true;

            } else {

                // first time resolving this artifact

                ArtifactResolutionRequest request = new ArtifactResolutionRequest();
                request.setForceUpdate(true);
                request.setRemoteRepositories(Collections.singletonList(repo));

                if (proxy != null) {

                    request.setProxies(Collections.singletonList(proxy));

                }
                request.setArtifact(artifact);

                ArtifactResolutionResult resolutionResult = artifactResolver.resolve(request);

                found = resolutionResult.isSuccess();

            }

            if (found) {

                if (verbose) {
                    log.info(" - [ resolved ] " + artifact + (inCache ? " (from cache)" : ""));
                }
                result.add(artifact);
                ids.add(id);

            } else {

                if (log.isDebugEnabled()) {
                    log.debug(" - [unresolved] " + artifact);
                }

            }
        }

        return result;

    }

    protected String getArtifactId(Artifact artifact) {
        String id = artifact.getGroupId() + ":" +
                    artifact.getArtifactId() + ":" +
                    artifact.getVersion();
        return id;
    }

    protected List<String> getRepositoryCache(String url) {
        if (resolved == null) {
            resolved = new TreeMap<>();
        }
        List<String> ids = resolved.get(url);
        if (ids == null) {
            ids = new ArrayList<>();
            resolved.put(url, ids);
        }
        return ids;
    }


//    protected Wagon getWagon(ArtifactRepository repo) throws Exception {
//
//        Repository repository = new Repository(repo.getId(), repo.getUrl());
//        Wagon wagon = wagonManager.getWagon(repository);
//
//        wagon.setTimeout(1000);
//
//        if (getLog().isDebugEnabled()) {
//            Debug debug = new Debug();
//
//            wagon.addSessionListener(debug);
//            wagon.addTransferListener(debug);
//        }
//
//        // FIXME when upgrading to maven 3.x : this must be changed.
//        AuthenticationInfo auth = wagonManager.getAuthenticationInfo(repo.getId());
//
//        ProxyInfo proxyInfo = getProxyInfo();
//        if (proxyInfo != null) {
//            wagon.connect(repository, auth, proxyInfo);
//        } else {
//            wagon.connect(repository, auth);
//        }
//
//        return wagon;
//    }
//
//    protected void disconnect(Wagon wagon) {
//        try {
//            wagon.disconnect();
//        } catch (ConnectionException e) {
//            Log log = getLog();
//            if (log.isDebugEnabled()) {
//                log.error("Error disconnecting wagon - ignored", e);
//            } else {
//                log.error("Error disconnecting wagon - ignored");
//            }
//        }
//    }

    protected ProxyInfo getProxyInfo() {
        ProxyInfo proxyInfo = null;
        if (proxy != null && !StringUtils.isEmpty(proxy.getHost())) {

            proxyInfo = new ProxyInfo();
            proxyInfo.setHost(proxy.getHost());
            proxyInfo.setType(proxy.getProtocol());
            proxyInfo.setPort(proxy.getPort());
            proxyInfo.setNonProxyHosts(proxy.getNonProxyHosts());
            proxyInfo.setUserName(proxy.getUsername());
            proxyInfo.setPassword(proxy.getPassword());
        }

        return proxyInfo;
    }

    protected boolean isGoalSkip() {
        return skipCheckAutocontainer;
    }

}
