package org.nuiton.plugin;

/*
 * #%L
 * Helper Maven Plugin :: API
 * %%
 * Copyright (C) 2009 - 2012 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.shared.dependency.graph.DependencyNode;
import org.apache.maven.shared.dependency.graph.traversal.CollectingDependencyNodeVisitor;
import org.apache.maven.shared.dependency.graph.traversal.DependencyNodeVisitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Une classe de methodes utiles sur les dependences entre artifacts.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 0.5
 */
public class DependencyUtil {

    /** Logger. */
    private static final Log log = LogFactory.getLog(DependencyUtil.class);

    public static void sortArtifacts(DependencyNode rootNode, List<Artifact> artifacts, boolean verbose) {

        if (artifacts.size() < 2) {
            return;
        }

        Map<String, ArtifactDependencyEntry> dico = new HashMap<>(artifacts.size());

        for (Artifact a : artifacts) {
            ArtifactDependencyEntry entry = new ArtifactDependencyEntry(a);
            dico.put(entry.artifactKey, entry);
        }

        // contient les artifacts non encore fixes
        Set<String> universe = new HashSet<>(dico.keySet());

        // recupere les noeuds pour chaque artifact en une seule passe

        setNodes(rootNode, dico, universe, verbose);

        // contient les artifacts resouds dans l'ordre de dependances
        List<String> parsed = new ArrayList<>();

        // premiere passe pour recuperer l'ensemble des dependances
        for (ArtifactDependencyEntry entry : dico.values()) {
            entry.depIds = getDependencies(/*rootNode,*/ entry, universe, verbose);
        }

        int level = 0;
        do {
            if (verbose) {
                log.info("run into level " + level++);
            }
            // on parcourt les artifacts pour detecter les nouveau
            // artifacts fixes
            List<String> levelFixed = new ArrayList<>();

            for (String key : universe) {
                ArtifactDependencyEntry entry = dico.get(key);
                // cet artifact n'est pas encore fixe
                if (entry.depIds.isEmpty()) {
                    // plus de dependance pour cet artifact
                    levelFixed.add(key);
                    if (verbose) {
                        log.info("fixed artifact " + key);
                    }
                }
            }

            if (levelFixed.isEmpty()) {
                // aucune modification, c'est un cycle!
                throw new IllegalStateException("cycle detecte ! entre les artifacts " + universe);
            }

            // on met a jour les listes
            universe.removeAll(levelFixed);
            parsed.addAll(levelFixed);

            if (universe.isEmpty()) {
                // tout a ete resolu, plus rien a faire
                break;
            }

            // on supprime les dependances fixees a ce niveau
            for (String key : universe) {
                ArtifactDependencyEntry entry = dico.get(key);
                entry.depIds.removeAll(levelFixed);
            }

            levelFixed.clear();
        } while (true);

        artifacts.clear();
        for (String key : parsed) {
            artifacts.add(dico.get(key).artifact);
        }

    }

    protected static String getArtifactId(Artifact artifact) {
        return artifact.getArtifactId() + ":" + artifact.getGroupId();
    }

    protected static List<String> getDependencies(
            /*DependencyNode rootNode,*/
            ArtifactDependencyEntry entry,
            Set<String> universe,
            boolean verbose) {

        List<String> order = new ArrayList<>();
        Set<String> exclude = new HashSet<>();

        if (verbose) {
            log.info("start [" + entry.artifactKey + "]");
        }
        DependencyNode node = entry.node;

        getDependencies(/*rootNode,*/
                        node,
                        entry.artifactKey,
                        universe,
                        verbose,
                        order,
                        exclude
        );

        return order;
    }

    private static void getDependencies(/*DependencyNode rootNode,*/
                                        DependencyNode node,
                                        final String artifactKey,
                                        final Set<String> universe,
                                        final boolean verbose,
                                        final List<String> order,
                                        final Set<String> exclude) {

        DependencyNodeVisitor visitor = new DependencyNodeVisitor() {
            @Override
            public boolean visit(DependencyNode node) {

                Artifact artifact = node.getArtifact();
                String key = getArtifactId(artifact);
                if (artifactKey.equals(key)) {
                    // artifact du noeud en parametre, rien a faire
                    return false;
                }
                if (order.contains(key) || exclude.contains(key)) {
                    // artifact deja rencontree
                    return false;
                }
                if (universe.contains(key)) {
                    // artifact a retenir
                    if (verbose) {
                        log.info(" <<  [" + node.getArtifact() + "]");
                    }
                    order.add(key);
                    return true;
                }

                exclude.add(key);
                return false;
            }

            @Override
            public boolean endVisit(DependencyNode node) {
                return false;
            }
        };

        node.accept(visitor);

//        for (Iterator<?> itr = node.preorderIterator(); itr.hasNext();) {
//            DependencyNode d = (DependencyNode) itr.next();
//            Artifact artifact = d.getArtifact();
//            String key = getArtifactId(artifact);
//            if (artifactKey.equals(key)) {
//                // artifact du noeud en parametre, rien a faire
//                continue;
//            }
//            if (order.contains(key) || exclude.contains(key)) {
//                // artifact deja rencontree
//                continue;
//            }
//
//            if (d.getState() != DependencyNode.INCLUDED) {
//                // on doit recuperer le noeud complete
//                if (log.isDebugEnabled()) {
//                    log.debug("!!! doit recuperer le noeud complet pour " +
//                              d.getArtifact());
//                }
//                DependencyNode node1 = getNode(rootNode, key, verbose);
//                getDependencies(rootNode,
//                                node1,
//                                artifactKey,
//                                universe,
//                                verbose,
//                                order,
//                                exclude
//                );
//            }
//
//            if (log.isDebugEnabled()) {
//                log.debug("[" + artifactKey + "] ???????  [" + key + "]");
//            }
//
//            if (universe.contains(key)) {
//                // artifact a retenir
//                if (verbose) {
//                    log.info(" <<  [" + d.getArtifact() + "]");
//                }
//                order.add(key);
//                continue;
//            }
//
//            // cet artifact peut etre marque comme a ne plus etre scanne
//            exclude.add(key);
//        }

    }

    protected static void setNodes(DependencyNode rootNode,
                                   Map<String, ArtifactDependencyEntry> dico,
                                   Set<String> universe,
                                   boolean verbose) {

        CollectingDependencyNodeVisitor visitor = new CollectingDependencyNodeVisitor();
        rootNode.accept(visitor);
        for (DependencyNode node : visitor.getNodes()) {
            Artifact artifact = node.getArtifact();
            String key = getArtifactId(artifact);
            if (log.isDebugEnabled()) {
                log.debug("key : " + key);
            }
            if (universe.contains(key) /* FIXME && d.getState() == DependencyNode.INCLUDED */) {
                ArtifactDependencyEntry entry = dico.get(key);
                entry.node = node;
                if (verbose || log.isDebugEnabled()) {
                    log.debug("detected node : " + node);
                }
            }
        }
//        for (Iterator<?> itr = rootNode.preorderIterator(); itr.hasNext();) {
//            DependencyNode d = (DependencyNode) itr.next();
//            Artifact artifact = d.getArtifact();
//            String key = getArtifactId(artifact);
//            if (log.isDebugEnabled()) {
//                log.debug("key : " + key);
//            }
//            if (universe.contains(key) &&
//                d.getState() == DependencyNode.INCLUDED) {
//                ArtifactDependencyEntry entry = dico.get(key);
//                entry.node = d;
////                if (d == null) {
////                    // ce cas ne devrait jamais arrive
////                    throw new IllegalStateException(
////                            "on a pas trouve le node pour l'artifact " +
////                            artifact);
////                }
//                if (log.isDebugEnabled()) {
//                    log.debug("detected node : " + d);
//                }
//            }
//        }
    }

//    protected static DependencyNode getNode(DependencyNode rootNode, String requiredKey, boolean verbose) {
//
//        for (Iterator<?> itr = rootNode.preorderIterator(); itr.hasNext();) {
//            DependencyNode d = (DependencyNode) itr.next();
//            Artifact artifact = d.getArtifact();
//            String key = getArtifactId(artifact);
//            if (log.isDebugEnabled()) {
//                log.debug("key : " + key);
//            }
//            if (requiredKey.equals(key) &&
//                d.getState() == DependencyNode.INCLUDED) {
//                return d;
//            }
//        }
//
//        return null;
//    }

    public static class ArtifactDependencyEntry {

        protected final Artifact artifact;

        protected final String artifactKey;

        protected DependencyNode node;

        protected List<String> depIds;

        public ArtifactDependencyEntry(Artifact artifact) {
            this.artifact = artifact;
            artifactKey = getArtifactId(artifact);
        }
    }
}
