package org.nuiton.plugin;

/*
 * #%L
 * Helper Maven Plugin :: Test Api
 * %%
 * Copyright (C) 2009 - 2015 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.maven.plugin.testing.MojoRule;
import org.apache.maven.project.DefaultProjectBuildingRequest;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.ProjectBuilder;
import org.apache.maven.project.ProjectBuildingRequest;
import org.apache.maven.project.ProjectBuildingResult;
import org.eclipse.aether.DefaultRepositorySystemSession;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.beans.Introspector;
import java.io.File;

/**
 * TODO Review this documentation.
 *
 * To offer inside each test method (annotated by a {@link Test}) the following properties :
 *
 * <ul>
 * <li>{@link #testDirectory} : location where to find resources for the test</li>
 * <li>{@link #pomFile} : location of the pom file to use to build the mojo</li>
 * <li>{@link #mojo} : the instanciated and initialized mojo</li>
 * </ul>
 */
public class MojoTestRule<P extends AbstractPlugin> extends MojoRule {

    /** Logger. */
    private static final Log log = LogFactory.getLog(MojoTestRule.class);

    /**
     * Directory where the resources for the current test.
     */
    private File testDirectory;

    /**
     * Method name of the current test.
     */
    private String testMethodName;

    /**
     * Pom file of the current test.
     */
    private File pomFile;

    /**
     * Mojo to used in the current test method.
     */
    private P mojo;

    /**
     * Class of the test.
     */
    private final Class<?> testClass;

    /**
     * Goal name of the mojo to test.
     */
    private final String goalName;

    public static <P extends AbstractPlugin> MojoTestRule<P> newRule(Class<?> testClass, String goalName) {
        return new MojoTestRule<>(testClass, goalName);
    }

    protected MojoTestRule(Class<?> testClass, String goalName) {
        this.testClass = testClass;
        this.goalName = goalName;
    }

    @Override
    public Statement apply(final Statement base, Description description) {

        testMethodName = description.getMethodName();

        return super.apply(base, description);

    }

    @SuppressWarnings("unchecked")
    protected P createMojo(File pomFile, String goalName) throws Exception {
        P result = (P) lookupMojo(goalName, pomFile);
        return result;
    }

    /**
     * Initialize the given mojo.
     *
     * @param mojo    the instanciate mojo
     * @param pomFile the pom file used to instanciate the mojo
     * @throws Exception if any pb
     */
    protected void setUpMojo(P mojo, File pomFile) throws Exception {

        MavenProject project = mojo.getProject();
        if (project == null) {

            log.debug("init maven project");
            ProjectBuilder projectBuilder = lookup(ProjectBuilder.class);
            ProjectBuildingRequest projectBuildingRequest = new DefaultProjectBuildingRequest();
            projectBuildingRequest.setRepositorySession(new DefaultRepositorySystemSession());
            ProjectBuildingResult projectBuildingResult = projectBuilder.build(pomFile, projectBuildingRequest);
            project = projectBuildingResult.getProject();

            mojo.setProject(project);
        }

        mojo.getProject().setFile(pomFile);

    }

    public boolean initMojo(P mojo) {

        boolean canContinue;
        try {

            mojo.init();
            canContinue = true;

        } catch (Exception e) {

            canContinue = false;

            if (log.isDebugEnabled()) {
                log.debug("Could not init mojo ", e);
            }

        }

        return canContinue;

    }

    protected void tearDownMojo(P mojo) {

        // by default nothing to clear

    }

    @Override
    protected void before() throws Throwable {

        log.info("Starting test: " + testClass.getName() + "#" + testMethodName);

        testDirectory = getTestDir(testMethodName);

        pomFile = getPomFile(testDirectory, testMethodName);

        try {

            Assert.assertTrue("could not find pom " + pomFile.getAbsoluteFile(), pomFile.exists());

            mojo = createMojo(pomFile, goalName);

            setUpMojo(mojo, pomFile);

        } catch (Exception ex) {

            throw new IllegalStateException("could not init test " + testClass + "#" + testMethodName, ex);

        }

    }

    @Override
    protected void after() {

        if (mojo != null) {
            tearDownMojo(mojo);
        }
        pomFile = null;
        mojo = null;
        testDirectory = null;

    }

    public P getMojo() {
        return mojo;
    }

    public File getPomFile() {
        return pomFile;
    }

    public File getTestDirectory() {
        return testDirectory;
    }

    /**
     * Obtain the location of the directory where to find resources for the next test.
     *
     * By convention, will be the package named by the test class name from the {@link TestHelper#getTestBasedir()}.
     *
     * @param methodName the method of the next test to execute
     * @return the directory where to find resources for the test
     */
    protected File getTestDir(String methodName) {

        //TC-20091101 use a decipatilize simple name to avoid conflict of package with existing class name.
        String rep = testClass.getPackage().getName() + "." + Introspector.decapitalize(testClass.getSimpleName());
        String[] paths = rep.split("\\.");
        File directory = TestHelper.getFile(TestHelper.getTestBasedir(), paths);
        if (TestHelper.isVerbose()) {
            log.info("test dir = " + getRelativePathFromBasedir(directory));
        } else if (log.isDebugEnabled()) {
            log.debug("test dir = " + getRelativePathFromBasedir(directory));
        }

        return directory;
    }

    /**
     * Obtain the location of the pom file to use for next mojo test.
     *
     * By default, the pom file is the file with name {@code methodName+".xml"} in the {@code testDir}.
     *
     * @param testDir    the location of resources for the next test (is the
     *                   result of the method {@link #getTestDir(String)}.
     * @param methodName the name of the next test
     * @return the location of the pom file for the next mojo test.
     */
    protected File getPomFile(File testDir, String methodName) {

        File pom = new File(testDir, methodName + ".xml");

        if (TestHelper.isVerbose()) {
            log.info("pom file = " + getRelativePathFromBasedir(pom));
        } else if (log.isDebugEnabled()) {
            log.debug("pom file = " + getRelativePathFromBasedir(pom));
        }

        return pom;
    }

    public String getRelativePathFromBasedir(File f) {
        return TestHelper.getRelativePath(TestHelper.getBasedir(), f);
    }

}
