package org.nuiton.helper.plugin;

/*
 * #%L
 * Helper Maven Plugin :: Mojos
 * %%
 * Copyright (C) 2009 - 2012 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.artifact.manager.WagonManager;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.apache.maven.settings.Proxy;
import org.apache.maven.settings.Server;
import org.apache.maven.settings.Settings;
import org.apache.maven.wagon.CommandExecutor;
import org.apache.maven.wagon.ConnectionException;
import org.apache.maven.wagon.Wagon;
import org.apache.maven.wagon.authentication.AuthenticationInfo;
import org.apache.maven.wagon.observers.Debug;
import org.apache.maven.wagon.proxy.ProxyInfo;
import org.apache.maven.wagon.repository.Repository;
import org.nuiton.plugin.AbstractPlugin;

/**
 * Mojo to execute on a remote server some scripts.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.3.1
 */
@Mojo(name = "execute-remote-command",
        defaultPhase = LifecyclePhase.VALIDATE,
        requiresOnline = true,
        requiresProject = true)
public class ExecuteRemoteCommandMojo extends AbstractPlugin {

    /**
     * A flag to activate verbose mode.
     */
    @Parameter(property = "helper.verbose", defaultValue = "${maven.verbose}")
    protected boolean verbose;

    /**
     * Project.
     */
    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    protected MavenProject project;

    /**
     * Active proxy from settings (if any).
     */
    @Parameter(defaultValue = "${settings.activeProxy}", required = true, readonly = true)
    protected Proxy proxy;

    /**
     * Settings.
     *
     * @since 1.1.0
     */
    @Parameter(defaultValue = "${settings}", required = true, readonly = true)
    protected Settings settings;

    /**
     * Id of the server to use to obtain user login.
     *
     * Must be found in your settings.xml.
     */
    @Parameter(property = "helper.repositoryId", required = true)
    protected String repositoryId;

    /**
     * Url where to execute script.
     *
     * The url must contains the wagon protocol (says scp:... or scpexe:...).
     */
    @Parameter(property = "helper.repositoryUrl", required = true)
    protected String repositoryUrl;

    /**
     * Command to execute on remote server.
     */
    @Parameter(property = "helper.command", required = true)
    protected String command;

    /**
     * Wagon manager component.
     */
    @Component
    protected WagonManager wagonManager;

    private CommandExecutor commandExecutor;

    @Override
    protected void init() throws Exception {

        Log log = getLog();

        if (log.isDebugEnabled()) {

            // always be verbose in debug mode
            setVerbose(true);
        }

        if (StringUtils.isEmpty(repositoryId)) {
            throw new MojoExecutionException("No 'repositoryId' defined.");
        }

        Server server = settings.getServer(repositoryId.trim());

        if (server == null) {
            throw new MojoExecutionException(
                    "Could not find server with id '" + repositoryId + "', check your settings.xml file.");
        }

        Repository repository = new Repository(repositoryId, repositoryUrl);
        Wagon wagon = getWagon(repository);

        if (!(wagon instanceof CommandExecutor)) {

            disconnect(wagon);

            throw new MojoExecutionException(
                    "The wagon " + wagon + " is not a command executor, will not be able to execute command.");
        }

        commandExecutor = (CommandExecutor) wagon;
    }

    @Override
    protected void doAction() throws Exception {

        if (isVerbose()) {
            getLog().info("Will execute command : " + command);
        }
        try {
            commandExecutor.executeCommand(command);
        } finally {

            disconnect(commandExecutor);
        }
    }

    @Override
    public MavenProject getProject() {
        return project;
    }

    @Override
    public void setProject(MavenProject project) {
        this.project = project;
    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    protected Wagon getWagon(Repository repository) throws Exception {

        Wagon wagon = wagonManager.getWagon(repository);

        wagon.setTimeout(1000);

        if (getLog().isDebugEnabled()) {
            Debug debug = new Debug();

            wagon.addSessionListener(debug);
            wagon.addTransferListener(debug);
        }

        // FIXME when upgrading to maven 3.x : this must be changed.
        AuthenticationInfo auth = wagonManager.getAuthenticationInfo(repository.getId());

        ProxyInfo proxyInfo = getProxyInfo();
        if (proxyInfo != null) {
            wagon.connect(repository, auth, proxyInfo);
        } else {
            wagon.connect(repository, auth);
        }

        return wagon;
    }

    protected void disconnect(Wagon wagon) {
        try {
            wagon.disconnect();
        } catch (ConnectionException e) {
            Log log = getLog();
            if (log.isDebugEnabled()) {
                log.error("Error disconnecting wagon - ignored", e);
            } else {
                log.error("Error disconnecting wagon - ignored");
            }
        }
    }

    protected ProxyInfo getProxyInfo() {
        ProxyInfo proxyInfo = null;
        if (proxy != null && !StringUtils.isEmpty(proxy.getHost())) {

            proxyInfo = new ProxyInfo();
            proxyInfo.setHost(proxy.getHost());
            proxyInfo.setType(proxy.getProtocol());
            proxyInfo.setPort(proxy.getPort());
            proxyInfo.setNonProxyHosts(proxy.getNonProxyHosts());
            proxyInfo.setUserName(proxy.getUsername());
            proxyInfo.setPassword(proxy.getPassword());
        }

        return proxyInfo;
    }
}
