package org.nuiton.helper.plugin;

/*
 * #%L
 * Helper Maven Plugin :: Mojos
 * %%
 * Copyright (C) 2009 - 2012 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.metadata.ArtifactMetadata;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.nuiton.plugin.AbstractPlugin;
import org.nuiton.plugin.PluginHelper;
import org.nuiton.plugin.PluginWithEncoding;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Collect files some files from a project and copy them into a directory.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1.0
 */
@Mojo(name = "collect-files", requiresProject = true)
public class CollectFilesMojo extends AbstractPlugin implements PluginWithEncoding {

    /**
     * The projects in the reactor.
     *
     * @since 1.2.4
     */
    @Parameter(property = "reactorProjects", readonly = true, required = true)
    protected List<?> reactorProjects;

    /**
     * Fallback maven project (for maven 2).
     *
     * @since 1.0.3
     */
    @Parameter( defaultValue = "${project}", required = true, readonly = true )
    protected MavenProject project;

    /**
     * The artifacts to publish from the project build.
     *
     * @since 1.0.0
     */
    @Parameter(property = "project.attachedArtifacts", readonly = true, required = true)
    protected List<?> attachedArtifacts;

    /**
     * The local repository.
     *
     * @since 1.2.2
     */
    @Parameter(property = "localRepository", readonly = true, required = true)
    protected ArtifactRepository localRepository;

    /**
     * The remote repository.
     *
     * @since 1.2.2
     */
    @Parameter(property = "project.distributionManagementArtifactRepository", readonly = true, required = true)
    protected ArtifactRepository deploymentRepository;

    /**
     * Project artifact.
     *
     * @since 1.2.2
     */
    @Parameter(property = "project.artifact", readonly = true, required = true)
    protected Artifact artifact;

    /**
     * User extra files to collect.
     *
     * Multi values can be used, separated by comma.
     *
     * @since 1.0.0
     */
    @Parameter(property = "helper.extraFiles")
    protected String extraFiles;

    /**
     * File name pattern of selected files to publish.
     *
     * If no Set - no include filter
     *
     * @since 1.0.0
     */
    @Parameter(property = "helper.includes", defaultValue = "")
    protected String includes;

    /**
     * File name pattern of selected files to publish.
     *
     * If no Set - no exclude filter
     *
     * @since 1.0.0
     */
    @Parameter(property = "helper.excludes", defaultValue = "")
    protected String excludes;

    /**
     * Directory where to store collected files.
     *
     * <b>Note :</b> In a multi-module context, will always use the value of
     * the property of the root module, because we need to push collected files
     * to only one place.
     *
     * @since 1.0.0
     */
    @Parameter(property = "helper.outputDirectory", defaultValue = "target/collect")
    protected String outputDirectory;

    /**
     * File with all files collected (one file by line in absolute path).
     *
     * <b>Note :</b> If not Set, will not generate the description file.
     *
     * @since 1.0.0
     */
    @Parameter(property = "helper.descriptionFile")
    protected String descriptionFile;

    /**
     * Un flag pour collecter aussi les fichiers attaches au projet.
     *
     * @since 1.0.0
     */
    @Parameter(property = "helper.includeAttached", defaultValue = "true")
    protected boolean includeAttached;

    /**
     * Un flag pour collecter aussi les fichiers des sites attaches au projet.
     *
     * @since 1.2.2
     */
    @Parameter(property = "helper.includeSiteAttached", defaultValue = "false")
    protected boolean includeSiteAttached;

    /**
     * Un flag pour recopier les fichiers collectés.
     *
     * @since 1.0.0
     */
    @Parameter(property = "helper.copyFiles", defaultValue = "true")
    protected boolean copyFiles;

    /**
     * Un flag pour activer le mode verbeux.
     *
     * @since 1.0.0
     */
    @Parameter(property = "helper.verbose", defaultValue = "${maven.verbose}")
    protected boolean verbose;

    /**
     * A flag to skip the goal.
     *
     * @since 1.0.0
     */
    @Parameter(property = "helper.skip", defaultValue = "false")
    protected boolean skip;

    /**
     * Un flag pour activer le mode verbeux.
     *
     * @since 1.0.0
     */
    @Parameter(property = "helper.dryRun", defaultValue = "false")
    protected boolean dryRun;

    /**
     * Encoding a utiliser pour lire et ecrire les fichiers.
     *
     * @since 1.0.0
     */
    @Parameter(property = "helper.encoding", defaultValue = "${project.build.sourceEncoding}", required = true)
    protected String encoding;

    /** Files to collect */
    protected List<File> files;

    @Override
    protected void init() throws Exception {
        if (skip) {
            return;
        }
        if (getLog().isDebugEnabled()) {
            setVerbose(true);
        }

        files = getFiles();
    }

    @Override
    protected boolean checkSkip() {
        if (skip) {
            getLog().info("Skipping goal (skip flag is on).");
            return false;
        }

        if (files.isEmpty()) {
            getLog().warn("Skipping goal (No file to collect).");
            return false;
        }

        return super.checkSkip();
    }

    @Override
    public void doAction() throws Exception {

        if (isVerbose()) {
            getLog().info("project = " + project);
        }

        MavenProject rootProject = getRootProject(project, reactorProjects);

        if (rootProject == null) {

            throw new MojoFailureException("Could not find root project for " + project + " in reactor " + reactorProjects);
        }

        if (isVerbose()) {
            getLog().info("root project = " + rootProject);
        }

        File base = new File(rootProject.getBasedir(), outputDirectory);

        File output = new File(base, project.getGroupId() + "--" + project.getArtifactId());

        if (verbose) {
            getLog().info("Ouput dir : " + output);
        }

        if (dryRun) {
            getLog().info("\n  dryRun flag is on, no file will be copied!\n");
        } else {

            if (copyFiles) {
                createDirectoryIfNecessary(output);
            }
        }

        File description = null;

        List<File> incomingFiles = null;

        boolean withDescriptionFile = false;

        if (StringUtils.isNotBlank(descriptionFile)) {

            description = new File(descriptionFile);

            if (!description.isAbsolute()) {

                // relativize from root project
                description = new File(rootProject.getBasedir(), descriptionFile);
            }

            withDescriptionFile = true;

            if (description.exists()) {
                // reload existing
                try {
                    incomingFiles = getFiles(description);
                    getLog().info("Loaded " + description);
                } catch (IOException ex) {
                    throw new MojoExecutionException("could not load file " + description, ex);
                }
            } else {
                incomingFiles = new ArrayList<>();
            }
        }

        if (!withDescriptionFile && !copyFiles) {
            throw new MojoExecutionException("must use at least one of the parameters 'copyFiles' or 'descriptionFile'");
        }

        String basedir = rootProject.getBasedir().getAbsolutePath();
        int basedirLength = basedir.length();
        for (File f : files) {
            String absolutePath = f.getAbsolutePath();
            String path = absolutePath.substring(basedirLength + 1);
            File dst = new File(output, f.getName());

            if (withDescriptionFile) {
                if (copyFiles) {
                    incomingFiles.add(dst);
                } else {
                    incomingFiles.add(f);
                }
            }

            if (isVerbose()) {
                getLog().info("Collected file " + path);
            }
            if (!dryRun && copyFiles) {
                // copy the collected file
                copyFile(f, dst);

            }
        }

        if (!dryRun && withDescriptionFile) {
            try {
                setFiles(description, incomingFiles);
                if (isVerbose()) {
                    getLog().info("Saved " + description);
                }
            } catch (IOException ex) {
                throw new MojoExecutionException("could not save file " + description, ex);
            }
        }
    }

    /**
     * Read a file containing on each line the path of a file.
     *
     * @param input the file where to pick files (one file per line).
     * @return the list of files read from the given file
     * @throws IOException if any pb while reading file
     */
    public List<File> getFiles(File input) throws IOException {
        List<File> result = new ArrayList<>();
        result.addAll(Arrays.asList(PluginHelper.getLinesAsFiles(input)));
        return result;
    }

    /**
     * Save the list of files in the given output file.
     *
     * Each line is the absolute path of each files of the list
     *
     * @param output the file when to write
     * @param files  the files to store
     * @throws IOException if any pb when writing file
     */
    public void setFiles(File output, List<File> files) throws IOException {
        StringBuilder builder = new StringBuilder();
        for (File f : files) {
            builder.append(f.getAbsolutePath()).append("\n");
        }
        writeFile(output, builder.toString(), encoding);
    }

    protected List<File> getFiles() {

        Pattern includePattern = StringUtils.isBlank(includes) ? null : Pattern.compile(includes.trim());
        Pattern excludePattern = StringUtils.isBlank(excludes) ? null : Pattern.compile(excludes.trim());

        List<File> result = new ArrayList<>();

        // attach the project mail file

        if (includeAttached) {

            if ("pom".equals(project.getPackaging())) {
                addFile(project.getFile(), "adding artifact file : ", result, includePattern, excludePattern);
            } else {

                addFile(artifact.getFile(), "adding artifact file : ", result, includePattern, excludePattern);
            }
        }
        File file;

        // attach extra files

        if (StringUtils.isNotBlank(extraFiles)) {

            File basedir = project.getBasedir();
            for (String path : extraFiles.split(",")) {
                path = path.trim();
                if (path.startsWith(basedir.getAbsolutePath())) {
                    file = new File(path.trim());
                } else {
                    file = new File(basedir, path.trim());
                }

                addFile(file, "adding user file : ", result, includePattern, excludePattern);

            }
        }

        // attach project attached files

        if (includeAttached && attachedArtifacts != null) {
            if (isVerbose()) {
                getLog().info("Will add build attached files.");
            }

            for (Object o : attachedArtifacts) {

                file = ((Artifact) o).getFile();
                addFile(file, "adding attached artifact file : ", result, includePattern, excludePattern);
            }
        }

        if (includeSiteAttached) {
            if (isVerbose()) {
                getLog().info("Will add site attached files.");
            }
            for (ArtifactMetadata o : artifact.getMetadataList()) {
                //TC-20100118 : we do not want to include maven-site-plugin
                // as dependencies
                //This is a big hack and we should sheck this name does not
                // changes when bumping maven version (anyway, it is not risky
                // since this class should never change ?)
                if ("SiteDescriptorArtifactMetadata".equals(o.getClass().getSimpleName())) {
                    String path = localRepository.pathOfLocalRepositoryMetadata(o, deploymentRepository);
                    File destination = new File(localRepository.getBasedir(), path);
                    addFile(destination, "adding site descriptor file : ", result, includePattern, excludePattern);
                }
            }
        }

        return result;
    }

    protected void addFile(File f, String msg, List<File> files, Pattern includePattern, Pattern excludePattern) {
        if (f == null) {
            return;
        }
        if (!f.exists()) {
            getLog().warn("skip unexisting file " + f + " (" + msg + ")");
            return;
        }
        if (excludePattern != null) {
            if (excludePattern.matcher(f.getName()).matches()) {
                // skip this file
                if (verbose) {
                    getLog().info("skip " + f + " : in excludes " + excludes);
                }
                return;
            }
        }
        if (includePattern != null) {
            if (!includePattern.matcher(f.getName()).matches()) {
                // skip this file
                if (verbose) {
                    getLog().info("skip " + f + " : not in includes " + includes);
                }
                return;
            }
        }
        if (verbose) {
            getLog().info(msg + f);
        }
        files.add(f);
    }

    @Override
    public MavenProject getProject() {
        return project;
    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setProject(MavenProject project) {
        this.project = project;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    @Override
    public String getEncoding() {
        return encoding;
    }

    @Override
    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }
}

