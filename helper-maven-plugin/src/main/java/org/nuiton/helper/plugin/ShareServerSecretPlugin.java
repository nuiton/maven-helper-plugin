package org.nuiton.helper.plugin;

/*
 * #%L
 * Helper Maven Plugin :: Mojos
 * %%
 * Copyright (C) 2009 - 2012 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.apache.maven.settings.Server;
import org.apache.maven.settings.Settings;
import org.nuiton.plugin.AbstractPlugin;
import org.sonatype.plexus.components.sec.dispatcher.SecDispatcher;

import java.util.EnumMap;
import java.util.Map;
import java.util.Properties;

/**
 * Obtain a server authentication and share it in the maven
 * project properties.
 *
 * To select data to export from the server with the given {@code serverId},
 * fill the properties :
 * <pre>
 *   usernameOut
 *   passwordOut
 *   privateKeyOut
 *   passphraseOut
 * </pre>
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.1.0
 */
@Mojo(name = "share-server-secret",
        defaultPhase = LifecyclePhase.INITIALIZE,
        requiresProject = true)
public class ShareServerSecretPlugin extends AbstractPlugin {

    /**
     * Project.
     *
     * @since 1.1.0
     */
    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    protected MavenProject project;

    /**
     * Settings.
     *
     * @since 1.1.0
     */
    @Parameter(defaultValue = "${settings}", required = true, readonly = true)
    protected Settings settings;

    /**
     * Server id to use for authentication (must be defined in your setting
     * and use the maven &gt;= 2.1.0 password encryption mecanism).
     *
     * @since 1.1.0
     */
    @Parameter(property = "helper.serverId", required = true)
    protected String serverId;

    /**
     * The name of the property where to export the username of the server.
     *
     * <b>Note:</b> If not set - then no export of the username of the server.
     *
     * @since 1.1.0
     */
    @Parameter
    protected String usernameOut;

    /**
     * The name of the property where to export the password of the server.
     *
     * <b>Note:</b> If not set - then no export of the password of the server.
     *
     * <b>Note:</b> If the password is crypted (since maven 2.1.0) then decrypt it.
     *
     * @since 1.1.0
     */
    @Parameter
    protected String passwordOut;

    /**
     * The name of the property where to export the passphrase of the server.
     *
     * <b>Note:</b> If not set - then no export of the passphrase of the server.
     *
     * <b>Note:</b> If the passphrase is crypted (since maven 2.1.0) then decrypt it.
     *
     * @since 1.1.0
     */
    @Parameter
    protected String passphraseOut;

    /**
     * The name of the property where to export the private key of the server.
     *
     * <b>Note:</b> If not set - then no export of the private key of the server.
     *
     * @since 1.1.0
     */
    @Parameter
    protected String privateKeyOut;

    /**
     * A flag to activate verbose mode.
     *
     * @since 1.1.0
     */
    @Parameter(property = "helper.verbose", defaultValue = "${maven.verbose}")
    protected boolean verbose;

    /**
     * A flag to execute only once the mojo.
     *
     * <b>Note:</b> By default, value is {@code true} since it is not necessary to inject twice secrets in session.
     *
     * @since 1.1.0
     */
    @Parameter(property = "helper.runOnce", defaultValue = "true")
    protected boolean runOnce;

    /**
     * A flag to restrict only to run on root module.
     *
     * @since 2.1
     */
    @Parameter(property = "helper.runOnlyOnRoot", defaultValue = "false")
    protected boolean runOnlyOnRoot;

    /**
     * A flag to skip the goal.
     *
     * @since 2.1
     */
    @Parameter(property = "helper.skipShareServerSecret", defaultValue = "false")
    protected boolean skipShareServerSecret;


    /**
     * A flag to skip null values.
     *
     * @since 1.6
     */
    @Parameter(property = "helper.skipNullValues", defaultValue = "false")
    protected boolean skipNullValues;

    /**
     * password decypher
     *
     * @since 1.1.0
     */
    @Component(hint = "maven-helper-plugin")
    protected SecDispatcher sec;

    /**
     * The server to use to obtain secrets.
     */
    private Server server;

    /**
     * Server secrets to expose.
     */
    private Map<Property, String> propertiesToTreate;

    public enum Property {
        username {
            @Override
            public String getSecret(Server server) {
                return server.getUsername();
            }
        },
        password {
            @Override
            public String getSecret(Server server) {
                return server.getPassword();
            }
        },
        passphrase {
            @Override
            public String getSecret(Server server) {
                return server.getPassphrase();
            }
        },
        privateKey {
            @Override
            public String getSecret(Server server) {
                return server.getPrivateKey();
            }
        };

        public abstract String getSecret(Server server);
    }

    @Override
    public void init() throws Exception {

        if (isGoalSkip()) {
            return;
        }

        propertiesToTreate = new EnumMap<>(Property.class);

        if (StringUtils.isNotEmpty(usernameOut)) {
            propertiesToTreate.put(Property.username, usernameOut);
        }
        if (StringUtils.isNotEmpty(passwordOut)) {
            propertiesToTreate.put(Property.password, passwordOut);
        }
        if (StringUtils.isNotEmpty(passphraseOut)) {
            propertiesToTreate.put(Property.passphrase, passphraseOut);
        }
        if (StringUtils.isNotEmpty(privateKeyOut)) {
            propertiesToTreate.put(Property.privateKey, privateKeyOut);
        }
        if (propertiesToTreate.isEmpty()) {
            throw new MojoExecutionException(
                    "Nothing to export, you should specify what to " +
                    "export via 'usernameOut', 'passwordOut', " +
                    "'passphraseOut' or 'privateKeyOut' parameters.");
        }

        if (StringUtils.isEmpty(serverId)) {
            throw new MojoExecutionException("No 'serverId' defined.");
        }

        server = settings.getServer(serverId.trim());

        if (server == null) {
            throw new MojoExecutionException(
                    "Could not find server with id '" + serverId + "', check your settings.xml file.");
        }
    }

    @Override
    public boolean checkSkip() {

        if (isGoalSkip()) {
            getLog().info("Skipping goal (skipShareServerSecret flag is on).");
            return false;
        }

        // check if plugin was already done
        StringBuilder buffer = new StringBuilder("share-secret##");
        buffer.append(serverId);
        buffer.append("##");
        for (Map.Entry<Property, String> entry :
                propertiesToTreate.entrySet()) {
            buffer.append(entry.getKey()).append('-').append(entry.getValue());
        }

        String key = buffer.toString();

        boolean shouldInvoke = needInvoke(runOnce, runOnlyOnRoot, key);
        if (!shouldInvoke) {
            getLog().info("Skipping goal (runOnce flag is on and goal was already executed).");
        }
        return shouldInvoke && super.checkSkip();

    }

    @Override
    protected void doAction() throws Exception {

        Properties properties = project.getModel().getProperties();

        for (Map.Entry<Property, String> entry :
                propertiesToTreate.entrySet()) {
            Property key = entry.getKey();
            String propertyValue = key.getSecret(server);
            String propertyTargetName = entry.getValue();

            if (isVerbose()) {
                getLog().info("will decrypt [" + key + "] : " + propertyValue);
            }
            propertyValue = sec.decrypt(propertyValue);

            if (propertyValue == null) {
                if (skipNullValues) {
                    getLog().warn("no export server [" + serverId + "] " + key.name() +
                                  " in ${" + propertyTargetName + "} (null value)");

                } else {
                    throw new MojoExecutionException(
                            "Null values found for server [" +
                            serverId + "] " + key.name() + ", use skipNullValue to authrize it.");
                }

            } else {

                getLog().info("Exporting server [" + serverId + "] " + key.name() + " in ${" + propertyTargetName + "}");

                properties.setProperty(propertyTargetName, propertyValue);
            }
        }
    }

    @Override
    public MavenProject getProject() {
        return project;
    }

    @Override
    public void setProject(MavenProject project) {
        this.project = project;
    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    public String getPassphraseOut() {
        return passphraseOut;
    }

    public void setPassphraseOut(String passphraseOut) {
        this.passphraseOut = passphraseOut;
    }

    public String getPasswordOut() {
        return passwordOut;
    }

    public void setPasswordOut(String passwordOut) {
        this.passwordOut = passwordOut;
    }

    public String getPrivateKeyOut() {
        return privateKeyOut;
    }

    public void setPrivateKeyOut(String privateKeyOut) {
        this.privateKeyOut = privateKeyOut;
    }

    public String getUsernameOut() {
        return usernameOut;
    }

    public void setUsernameOut(String usernameOut) {
        this.usernameOut = usernameOut;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public void setSec(SecDispatcher sec) {
        this.sec = sec;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    public void setRunOnce(boolean runOnce) {
        this.runOnce = runOnce;
    }

    protected boolean isGoalSkip() {
        return skipShareServerSecret;
    }
}
