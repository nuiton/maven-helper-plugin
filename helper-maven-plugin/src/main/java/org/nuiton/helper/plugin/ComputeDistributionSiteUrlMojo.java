package org.nuiton.helper.plugin;

/*
 * #%L
 * Helper Maven Plugin :: Mojos
 * %%
 * Copyright (C) 2009 - 2016 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.maven.model.DistributionManagement;
import org.apache.maven.model.Site;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.nuiton.plugin.AbstractPlugin;

import java.util.Properties;

/**
 * To compute distribution site url distinguising url when project version is a snapshot.
 *
 * Created on 27/02/16.
 *
 * @author Tony Chemit - chemit@codelutin.com
 * @since 2.3
 */
@Mojo(name = "compute-distribution-site-url",
        defaultPhase = LifecyclePhase.PRE_SITE,
        requiresOnline = true,
        requiresProject = true)
@Deprecated
public class ComputeDistributionSiteUrlMojo extends AbstractPlugin {

    public static final String SNAPSHOT_SUFFIX = "-SNAPSHOT";

    /**
     * Invoking project's version.
     */
    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    private MavenProject project;

    /**
     * Site deploy classifier to use when project's version is a snapshot.
     */
    @Parameter(property = "helper.distributionSiteUrlPrefix", defaultValue = "scpexe://forge.nuiton.org/var/lib/doc/maven-site/${platform}/${projectId}", required = true)
    private String distributionSiteUrlPrefix;

    /**
     * Site deploy classifier to use when project's version is a snapshot.
     */
    @Parameter(property = "helper.snapshotSiteDeployClassifier", defaultValue = "develop", required = true)
    private String snapshotSiteDeployClassifier;

    /**
     * Site deploy classifier to use when project's version is not a snapshot.
     */
    @Parameter(property = "helper.releaseSiteDeployClassifier", defaultValue = "${project.version}", required = true)
    private String releaseSiteDeployClassifier;

    /**
     * A flag to activate verbose mode.
     */
    @Parameter(property = "helper.verbose", defaultValue = "${maven.verbose}")
    private boolean verbose;

    private String siteDeployClassifier;

    private String distributionSiteUrl;

    @Override
    public boolean checkSkip() {
        return true;
    }

    @Override
    protected void init() throws Exception {

        if (project.getVersion().endsWith(SNAPSHOT_SUFFIX)) {

            siteDeployClassifier = snapshotSiteDeployClassifier;
            if (isVerbose()) {
                getLog().info("Use snapshot siteDeployClassifier: " + siteDeployClassifier);
            }

        } else {

            siteDeployClassifier = releaseSiteDeployClassifier;
            if (isVerbose()) {
                getLog().info("Use release siteDeployClassifier: " + siteDeployClassifier);
            }

        }

        if (project.isExecutionRoot()) {

            distributionSiteUrl = distributionSiteUrlPrefix.trim();
            if (!distributionSiteUrl.endsWith("/")) {
                distributionSiteUrl += "/";
            }
            distributionSiteUrl += siteDeployClassifier;

        } else {

            DistributionManagement distributionManagement = project.getParent().getDistributionManagement();
            Site site = distributionManagement.getSite();
            if (site == null) {
                getLog().error("parent's distributionManagement.site is absent, cannot get its site URL");
            } else {
                distributionSiteUrl = site.getUrl() + "/" + project.getArtifactId();
            }

        }

        if (isVerbose()) {
            getLog().info("distributionSiteUrl: " + distributionSiteUrl);
        }

    }

    @Override
    protected void doAction() throws Exception {

        Properties properties = project.getProperties();

        getLog().info("export siteDeployClassifier [" + siteDeployClassifier + "]");

        properties.setProperty("siteDeployClassifier", siteDeployClassifier);

        if (isVerbose()) {

            getLog().info("distributionManagement site url [" + distributionSiteUrl + "]");

        }

        DistributionManagement distributionManagement = project.getDistributionManagement();
        Site site = distributionManagement.getSite();
        if (site == null) {
            getLog().error("distributionManagement.site is absent, cannot set site URL");
        } else {
            site.setUrl(distributionSiteUrl);
        }

    }

    @Override
    public MavenProject getProject() {
        return project;
    }

    @Override
    public void setProject(MavenProject project) {
        this.project = project;
    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }
}
