/*
 * #%L
 * Helper Maven Plugin
 * %%
 * Copyright (C) 2009 - 2010 Tony Chemit, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.io;

import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.io.FileUpdaterHelper.JaxxFileUpdater;
import org.nuiton.plugin.PluginHelper;
import org.nuiton.plugin.TestHelper;

import java.io.File;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test of a {@link JaxxFileUpdater}  on the test sourcepath
 *
 * @author tchemit <chemit@codelutin.com>
 */
public class JaxxFileUpdaterTest {

    static MirroredFileUpdater fileUpdater;

    static File sourceFile;

    static File mirrorFile;

    @BeforeClass
    public static void initClass() throws Exception {

        File basedir = TestHelper.getBasedir();

        File src = TestHelper.getFile(basedir, "src", "test", "resources");
        File dir = TestHelper.getFile(basedir, "target", "test-classes");
        fileUpdater = FileUpdaterHelper.newJaxxFileUpdater(src, dir);

        // test source dir exists
        File sourceDirectory = fileUpdater.getSourceDirectory();
        assertTrue("sourcedir is null", sourceDirectory != null);
        assertTrue("sourcedir does not exists " + sourceDirectory, sourceDirectory.exists());

        // test destination dir exists
        File destinationDirectory = fileUpdater.getDestinationDirectory();
        assertTrue("destinationDirectory  is null", destinationDirectory != null);
        assertTrue("destinationDirectory  does not exists " + destinationDirectory, destinationDirectory.exists());

        sourceFile = TestHelper.getFile(fileUpdater.getSourceDirectory(), "org", "nuiton", "io", "JaxxDummy" + ".jaxx");

    }

    @Test
    public void testSourceFileIsUptoDate() throws Exception {

        assertTrue("fileUpdater was not init", fileUpdater != null);

        assertTrue("sourceFile was not init ", sourceFile != null);
        assertTrue("could not find source of this test " + sourceFile, sourceFile.exists());

        mirrorFile = fileUpdater.getMirrorFile(sourceFile);

        // we do not know if jaxx file is newer than the dummy java source ?

        PluginHelper.setLastModified(sourceFile, mirrorFile.lastModified() - 10);

        // java file is up to date since it was compiled to launch this test
        assertTrue(sourceFile + " should be uptodate", fileUpdater.isFileUpToDate(sourceFile));
    }

    @Test
    public void testSourceFileDoesNotHaveMirrorFile() throws Exception {

        assertTrue("fileUpdater was not init", fileUpdater != null);

        assertTrue("sourceFile was not init ", sourceFile != null);
        assertTrue("could not find source of this test " + sourceFile, sourceFile.exists());

        assertTrue("mirrorFile was not init ", mirrorFile != null);
        assertTrue("could not find mirrorFile " + mirrorFile, mirrorFile.exists());

        // now rename mirror Class, so source file is no more up to date
        File mirrorClassRenamed = new File(fileUpdater.getMirrorFile(sourceFile).getAbsolutePath() + "2");
        PluginHelper.renameFile(mirrorFile, mirrorClassRenamed);

        // java file is no more up to date
        assertFalse(sourceFile + " should not be uptodate", fileUpdater.isFileUpToDate(sourceFile));

        // renmae mirror Class to his original name
        PluginHelper.renameFile(mirrorClassRenamed, mirrorFile);

        // java file is now up to date (rename method does not affect lasmodified property on a file)
        assertTrue(sourceFile + " should be uptodate", fileUpdater.isFileUpToDate(sourceFile));
    }

    @Test
    public void testSourceFileIsNewerThanMirrorFile() throws Exception {

        assertTrue("fileUpdater was not init", fileUpdater != null);

        assertTrue("sourceFile was not init ", sourceFile != null);
        assertTrue("could not find source of this test " + sourceFile, sourceFile.exists());

        assertTrue("mirrorFile was not init ", mirrorFile != null);
        assertTrue("could not find mirrorFile " + mirrorFile, mirrorFile.exists());

        long sourceTime = sourceFile.lastModified();
        long mirrorTime = mirrorFile.lastModified();

        // make source file newer than mirror file
        PluginHelper.setLastModified(sourceFile, mirrorTime + 10);

        // java file is no more up to date
        assertFalse(sourceFile + " should not be uptodate", fileUpdater.isFileUpToDate(sourceFile));

        // put back old time to source file
        PluginHelper.setLastModified(sourceFile, sourceTime);

        // java file is now up to date (rename method does not affect lasmodified property on a file)
        assertTrue(sourceFile + " should be uptodate", fileUpdater.isFileUpToDate(sourceFile));
    }
}
