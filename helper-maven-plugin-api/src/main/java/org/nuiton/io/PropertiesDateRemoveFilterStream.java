package org.nuiton.io;

/*
 * #%L
 * Helper Maven Plugin :: API
 * %%
 * Copyright (C) 2009 - 2012 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Un ecrivain qui supprime la premiere ligne rencontree dans le flux.
 *
 * <b>Note: </b> Attention, les performance d'utilisation de cet ecrivain
 * est problèmatique, car sur de gros fichiers (&gt;1000 entrees) les
 * performances se degradent serieusement : pour 1200 entrees on arrive à
 * plus de 5 secondes, alors que sans on a 76 ms! ...
 *
 * FIXME : implanter quelque chose de plus performant dans tous les cas
 */
public class PropertiesDateRemoveFilterStream extends FilterOutputStream {

    private boolean firstLineOver;
    char endChar;

    public PropertiesDateRemoveFilterStream(OutputStream out) {
        super(out);
        firstLineOver = false;
        String lineSeparator = System.getProperty("line.separator");
        endChar = lineSeparator.charAt(lineSeparator.length() - 1);
    }

    @Override
    public void write(int b) throws IOException {
        if (!firstLineOver) {
            char c = (char) b;
            if (c == endChar) {
                firstLineOver = true;
            }
        } else {
            out.write(b);
        }
    }
}
