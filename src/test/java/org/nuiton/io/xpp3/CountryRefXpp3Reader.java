/*
 * #%L
 * Helper Maven Plugin
 * %%
 * Copyright (C) 2009 - 2012 Tony Chemit, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.io.xpp3;

import java.beans.IntrospectionException;

/**
 * To read {@link CountryRef}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.5
 */
public class CountryRefXpp3Reader extends AbstractXpp3Reader<CountryRef> {

    public CountryRefXpp3Reader() {
        this("countryRefs", "country");
    }

    public CountryRefXpp3Reader(String root, String tag) {
        super(CountryRef.class, root, tag);
    }

    @Override
    protected void initMappers() throws IntrospectionException {

        addAttributeValueMappers(DefaultDataConverter.Integer, true,
                                 "id");
    }
}
