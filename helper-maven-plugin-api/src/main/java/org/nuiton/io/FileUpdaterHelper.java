package org.nuiton.io;

/*
 * #%L
 * Helper Maven Plugin :: API
 * %%
 * Copyright (C) 2009 - 2012 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.File;

/** @author Tony Chemit - chemit@codelutin.com */
public class FileUpdaterHelper {

    static public MirroredFileUpdater newJavaFileUpdater() {
        return newJavaFileUpdater(null, null);
    }

    static public MirroredFileUpdater newJaxxFileUpdater() {
        return newJaxxFileUpdater(null, null);
    }

    static public MirroredFileUpdater newJavaFileUpdater(File src, File dst) {
        return new JavaFileUpdater(src, dst);
    }

    static public MirroredFileUpdater newJaxxFileUpdater(File src, File dst) {
        return new JaxxFileUpdater(src, dst);
    }

    /**
     * To test if a java source file is newser than his compiled class
     *
     * @author Tony Chemit - chemit@codelutin.com
     */
    public static class JavaFileUpdater extends MirroredFileUpdater {

        protected JavaFileUpdater(File sourceDirectory,
                                  File destinationDirectory) {
            super(".java", ".class", sourceDirectory, destinationDirectory);
        }

        @Override
        public File getMirrorFile(File f) {
            String file =
                    f.getAbsolutePath().substring(prefixSourceDirecotory);
            String mirrorRelativePath =
                    file.substring(0, file.length() - 4) + "class";
            return new File(destinationDirectory + File.separator +
                            mirrorRelativePath);
        }
    }

    /**
     * To test if a jaxx source file is newser than his generated java
     * source file.
     *
     * @author Tony Chemit - chemit@codelutin.com
     */
    public static class JaxxFileUpdater extends MirroredFileUpdater {

        protected JaxxFileUpdater(File sourceDirectory,
                                  File destinationDirectory) {
            super(".jaxx", ".java", sourceDirectory, destinationDirectory);
        }

        @Override
        public File getMirrorFile(File f) {
            String file =
                    f.getAbsolutePath().substring(prefixSourceDirecotory);
            String mirrorRelativePath =
                    file.substring(0, file.length() - 4) + "java";
            return new File(destinationDirectory + File.separator +
                            mirrorRelativePath);
        }
    }
}
