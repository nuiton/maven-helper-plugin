package org.nuiton.io;

/*
 * #%L
 * Helper Maven Plugin :: API
 * %%
 * Copyright (C) 2009 - 2012 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.File;

/**
 * Contract to be realized to test if a file is up to date.
 *
 * Use {@link #isFileUpToDate(File)} to determine if a file is up to date.
 *
 * @author Tony Chemit - chemit@codelutin.com
 */
public interface FileUpdater {

    /**
     * @param f file to test
     * @return {@code true} if file is up to date, {@code false} otherwise.
     */
    boolean isFileUpToDate(File f);

}
