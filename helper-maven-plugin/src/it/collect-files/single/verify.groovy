/*
 * #%L
 * Helper Maven Plugin :: Mojos
 * %%
 * Copyright (C) 2009 - 2012 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
// collect-files
assert new File(basedir, 'target/collect.properties').exists();
assert !new File(basedir, 'target/collect').exists();
content = new File(basedir, 'target/collect.properties').text;

separ = File.separator
prefix = 'target' + separ + 'its' + separ + 'collect-files' + separ + 'single' + separ;

assert content.contains(prefix + 'target' + separ + 'collect-files-single-0.jar');
assert content.contains(prefix + 'src' + separ + 'collect-files.txt');

// collect-files-2
assert new File(basedir, 'target/collect2.properties').exists();
assert new File(basedir, 'target/collect2').exists();
assert new File(basedir, 'target/collect2/org.nuiton.test--collect-files-single/collect-files-single-0.jar').exists();
assert new File(basedir, 'target/collect2/org.nuiton.test--collect-files-single/collect-files-2.txt').exists();

content = new File(basedir, 'target/collect2.properties').text;

prefix = 'target' + separ + 'its' + separ + 'collect-files' + separ + 'single' + separ + 'target' + separ + 'collect2' + separ;

assert content.contains(prefix + 'org.nuiton.test--collect-files-single' + separ + 'collect-files-single-0.jar');
assert content.contains(prefix + 'org.nuiton.test--collect-files-single' + separ + 'collect-files-2.txt');

// collect-files-3
assert !new File(basedir, 'target/collect3.properties').exists();
assert new File(basedir, 'target/collect3').exists();
assert new File(basedir, 'target/collect3/org.nuiton.test--collect-files-single/collect-files-single-0.jar').exists();

// collect-files-4
assert !new File(basedir, 'target/collect4.properties').exists();
assert !new File(basedir, 'target/collect4').exists();

// collect-files-5
assert !new File(basedir, 'target/collect5.properties').exists();
assert new File(basedir, 'target/collect5').exists();

assert !new File(basedir, 'target/collect5/org.nuiton.test--collect-files-single/collect-files-single-0.jar').exists();
assert new File(basedir, 'target/collect5/org.nuiton.test--collect-files-single/collect-files.txt').exists();
assert new File(basedir, 'target/collect5/org.nuiton.test--collect-files-single/collect-files-2.txt').exists();

// collect-files-6
assert !new File(basedir, 'target/collect6.properties').exists();
assert !new File(basedir, 'target/collect6').exists();

// collect-files-7
assert !new File(basedir, 'target/collect7.properties').exists();
assert !new File(basedir, 'target/collect7').exists();

return true;
