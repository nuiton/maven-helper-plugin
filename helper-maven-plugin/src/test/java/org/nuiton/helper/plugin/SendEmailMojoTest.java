package org.nuiton.helper.plugin;

/*
 * #%L
 * Helper Maven Plugin :: Mojos
 * %%
 * Copyright (C) 2009 - 2012 Codelutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.helper.io.mail.MailSender;
import org.nuiton.plugin.MojoTestRule;

import java.io.File;
import java.util.Collections;

/**
 * @author Tony Chemit - chemit@codelutin.com
 * @since 1.0.0
 */
public class SendEmailMojoTest {

    /** Logger. */
    private static final Log log = LogFactory.getLog(SendEmailMojoTest.class);

    @Rule
    public MojoTestRule<SendEmailMojo> rule = new MojoTestRule<SendEmailMojo>(SendEmailMojoTest.class, "send-email") {

        @Override
        protected void setUpMojo(SendEmailMojo mojo, File pomFile) throws Exception {

            super.setUpMojo(mojo, pomFile);

            // add a mailSender here
            MailSender sender = new MailSender();
            sender.setName("Nuiton Release Notification");
            sender.setEmail("test@noway.fr");
            mojo.setMailSender(sender);

            // add a destination
            mojo.setToAddresses(Collections.singletonList("chemit@codelutin.com"));

            canContinue = initMojo(mojo);

            if (canContinue) {
                if (mojo.isVerbose()) {
                    log.info("setup done for " + mojo.getProject().getFile().getName());
                }
            } else {
                log.error("setup was not successfull, will skip this test [" + getClass() + "]");
            }

        }

    };

    protected boolean canContinue;

    @Test
    public void sendEmail() throws Exception {

        Assume.assumeTrue(canContinue);
        SendEmailMojo mojo = rule.getMojo();
        Assert.assertNotNull(mojo);
        mojo.doAction();

    }

    @Test
    public void skipSendEmail() throws Exception {

        Assume.assumeTrue(canContinue);
        SendEmailMojo mojo = rule.getMojo();
        Assert.assertNotNull(mojo);
        mojo.execute();

    }

}
