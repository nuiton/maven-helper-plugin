/*
 * #%L
 * Helper Maven Plugin
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.io;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.plugin.PluginHelper;
import org.nuiton.plugin.TestHelper;

import java.io.File;
import java.net.URL;

/**
 * To test {@link SortedProperties}.
 *
 * @author tchemit <chemit@codelutin.com>
 * @since 1.3
 */
public class SortedPropertiesTest {


    /** Logger */
    static private Log log = LogFactory.getLog(SortedPropertiesTest.class);

    static File testDir;

    @BeforeClass
    public static void init() throws Exception {
        testDir = TestHelper.getTestDir(SortedPropertiesTest.class,
                                        "target" + File.separator + "surefire-workdir"
        );
        PluginHelper.createDirectoryIfNecessary(testDir);
    }

    /**
     * To test that a file loaded then stored keep unicode as they were previously.
     * <p/>
     * In fact we can have some {@code \u00E9} and {@code \u00e9}.
     *
     * @throws Exception if any error
     */
    @Test
    public void testUnicodeUpperInvolution() throws Exception {

        URL resource = getClass().getResource("unicodeUpper.txt");
        File in = new File(resource.toURI());

        log.info("Load file " + in);
        SortedProperties p = new SortedProperties("iso-8859-1", true);

        p.load(in);

        File out = new File(testDir, in.getName() + "~");

        log.info("Store file to " + out);

        p.store(out);

        String inStr = PluginHelper.readAsString(in, "iso-8859-1");
        String outStr = PluginHelper.readAsString(out, "iso-8859-1");

        Assert.assertEquals(inStr.trim(), outStr.trim());

    }

    /**
     * To test that a file loaded then stored keep unicode as they were previously.
     * <p/>
     * In fact we can have some {@code \u00E9} and {@code \u00e9}.
     *
     * @throws Exception if any error
     */
    @Test
    public void testUnicodeLowerInvolution() throws Exception {

        URL resource = getClass().getResource("unicodeLower.txt");
        File in = new File(resource.toURI());

        log.info("Load file " + in);
        SortedProperties p = new SortedProperties("iso-8859-1", true, true);

        p.load(in);

        File out = new File(testDir, in.getName() + "~");

        log.info("Store file to " + out);

        p.store(out);

        String inStr = PluginHelper.readAsString(in, "iso-8859-1");
        String outStr = PluginHelper.readAsString(out, "iso-8859-1");

        Assert.assertEquals(inStr.trim(), outStr.trim());
    }

    @Test
    public void testEncodingUTF8() throws Exception {

        URL resource = getClass().getResource("unicodeProperties.txt");
        File in = new File(resource.toURI());

        log.info("Load file " + in);
        SortedProperties p = new SortedProperties("utf-8");

        p.load(in);

        Assert.assertEquals(p.get("key"), "àéçèéù 汉字");

        File out = new File(testDir, "testEncodingUTF8.properties");

        log.info("Store file to " + out);

        p.store(out);

        String inStr = PluginHelper.readAsString(in, "utf-8");
        String outStr = PluginHelper.readAsString(out, "utf-8");

        Assert.assertEquals(inStr.trim(), outStr.trim());
    }

}
